<?php

/*
|--------------------------------------------------------------------------
| Register The Artisan Commands
|--------------------------------------------------------------------------
|
| Each available Artisan command must be registered with the console so
| that it is available to be called. We'll register every command so
| the console gets access to each of the command object instances.
|
*/

Artisan::add(new UpdateDatabaseCommand);
Artisan::add(new ImportOrderAddressCommand);
Artisan::add(new TransferDBDataCommand);
Artisan::add(new ClearInitAopDataCommand);
Artisan::add(new ResizeImagesCommand);
Artisan::add(new TransferArticlesDetailsCommand);
Artisan::add(new InsertOldLinksCommand);
Artisan::add(new GenerisaneKarakteristikeCommand);
