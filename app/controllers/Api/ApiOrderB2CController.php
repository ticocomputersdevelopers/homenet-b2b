<?php

class ApiOrderB2CController extends Controller {

	public function b2cOrders(){
        $data = Input::get();
        $rules = array(
        	'page' => 'integer|min:1',
        	'limit' => 'integer|max:1000',
            'id' => 'integer|min:1',
            'document_number' => 'max:20',
            'customer_id' => 'integer|exists:web_kupac,web_kupac_id',
            'customer_external_code' => 'max:255|exists:web_kupac,id_is',
            'status' => 'in:NEW,ACCEPTED,REALIZED,CANCELED',
            'date_from' => 'date_format:Y-m-d H:i:s'
        );
        $validator = Validator::make($data,$rules);
        if($validator->fails()){
        	return Response::json(['success'=>false,'messages'=>$validator->messages()],200);
        }

        $page = Input::get('page') ? intval(Input::get('page')) : 1;
        $limit = Input::get('limit') ? intval(Input::get('limit')) : 100;


		$ordersQuery = OrderB2C::with('customer','items.article','items.vat');
		if(!empty($id = Input::get('id'))){
			$ordersQuery = $ordersQuery->whereRaw("web_b2c_narudzbina_id = ".$id."");
		}
        if(!empty($documentNumber = Input::get('document_number'))){
            $ordersQuery = $ordersQuery->whereRaw("broj_dokumenta = '".$documentNumber."'");
        }
        if(!empty($customer_id = Input::get('customer_id'))){
            $ordersQuery = $ordersQuery->whereRaw("web_kupac_id = ".$customer_id."");
        }
		if(!empty($customerExternalCode = Input::get('customer_external_code'))){
			$ordersQuery = $ordersQuery->whereRaw("(select id_is from web_kupac where web_kupac.web_kupac_id = web_b2c_narudzbina.web_kupac_id limit 1) = '".$customerExternalCode."'");
		}
        if(!empty($status = Input::get('status'))){
            if($status == 'NEW'){
                $ordersQuery = $ordersQuery->whereRaw("prihvaceno = 0 AND realizovano = 0 AND stornirano = 0");
            }
            else if($status == 'ACCEPTED'){
                $ordersQuery = $ordersQuery->whereRaw("prihvaceno = 1 AND realizovano = 0 AND stornirano = 0");
            }
            else if($status == 'REALIZED'){
                $ordersQuery = $ordersQuery->whereRaw("realizovano = 1 AND stornirano = 0");
            }
            else if($status == 'CANCELED'){
                $ordersQuery = $ordersQuery->whereRaw("stornirano = 1");
            }
        }
		if(!empty($dateFrom = Input::get('date_from'))){
			$ordersQuery = $ordersQuery->whereRaw("poslednja_izmena::timestamp >= '".$dateFrom."'");
		}

		$orders = $ordersQuery->limit($limit)->offset((($page-1)*$limit))->orderBy('datum_dokumenta','desc')->get()->toArray();

		return Response::json(['success'=>true,'orders'=>$orders],200);
	}

	public function b2cOrderSave(){
        $data = Input::get();
        $regex = AdminSupport::regex();

        if(count($data) > 20000){
        	return Response::json(['success'=>false,'messages'=>'Max input vars is 20000.'],200);
        }
        if(DB::table('web_b2c_narudzbina')->count() > 5000){
        	return Response::json(['success'=>false,'messages'=>'Max number of categories is 5000.'],200);
        }

        foreach($data as $row){
	        $rules = array(
	        	'id' => 'integer|exists:web_b2c_narudzbina,web_b2c_narudzbina_id',
                'document_number' => 'max:20|exists:web_b2c_narudzbina,broj_dokumenta',
                'customer_id' => ((isset($row['id']) || isset($row['document_number'])) ? '' : 'required|').'integer|exists:web_kupac,web_kupac_id',
                'date' => 'date_format:Y-m-d H:i:s',
                'status' => 'in:NEW,ACCEPTED,REALIZED,CANCELED',
                'note' => 'max:2000',
                'items' => 'array'
	        );
	        $validator = Validator::make($row,$rules);
	        if($validator->fails()){
	        	return Response::json(['success'=>false,'messages'=>$validator->messages()],200);
	        	break;
	        }

            if(isset($row['items'])){
                foreach($row['items'] as $item){
                    $itemsRules = array(
                        'id' => 'integer|exists:web_b2c_narudzbina_stavka,web_b2c_narudzbina_stavka_id',
                        'sort' => 'integer|max:1000',
                        'article_id' => (isset($item['id']) ? '' : 'required|').'integer|exists:roba,roba_id',
                        'quantity' => (isset($item['id']) ? '' : 'required|').'numeric|digits_between:1,7',
                        'basic_price' => 'numeric|digits_between:1,15',
                        'vat_id' => (isset($item['id']) ? '' : 'required|').'integer|exists:tarifna_grupa,tarifna_grupa_id',
                        'unit_price' => 'numeric|digits_between:1,15'
                    );
                    $itemValidator = Validator::make($item,$itemsRules);
                    if($itemValidator->fails()){
                        return Response::json(['success'=>false,'messages'=>$itemValidator->messages()],200);
                        break;
                    }           
                }
            }            
        }

        foreach($data as $row){
        	$order = null;
        	if(isset($row['id'])){
	        	$order = OrderB2C::where('web_b2c_narudzbina_id',$row['id'])->first();
        	}else if(isset($row['document_number'])){
                $order = OrderB2C::where('broj_dokumenta',$row['document_number'])->first();
            }

        	if(is_null($order)){
	            $order = new OrderB2C();
                $br_nar = !is_null($br_doc = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id','!=','-1')->orderBy('web_b2c_narudzbina_id','desc')->pluck('broj_dokumenta')) ? intval(str_replace( 'WNB','',$br_doc)) + 1 : 1;
                $num_nar = str_pad($br_nar, 4, '0', STR_PAD_LEFT);
                $order->broj_dokumenta = "WNB".$num_nar;
	            $order->customer_id = $row['customer_id'];
                $order->datum_dokumenta = isset($row['date']) ? $row['date'] : date('Y-m-d H:i:s');
		    	$order->napomena = isset($row['note']) ? $row['note'] : null;

        	}else{
        		if(isset($row['customer_id'])){
		    		$order->customer_id = $row['customer_id'];
        		}
        		if(isset($row['date'])){
		    		$order->datum_dokumenta = $row['date'];
        		}
        		if(isset($row['note'])){
		    		$order->napomena = $row['note'];
        		}
        	}

            if(isset($row['status'])){
                if($row['status'] == 'NEW'){
                    $order->prihvaceno = 0;
                    $order->realizovano = 0;
                    $order->stornirano = 0;
                }
                else if($row['status'] == 'ACCEPTED'){
                    $order->prihvaceno = 1;
                    $order->realizovano = 0;
                    $order->stornirano = 0;
                }
                else if($row['status'] == 'REALIZED'){
                    $order->prihvaceno = 1;
                    $order->realizovano = 1;
                    $order->stornirano = 0;
                }
                else if($row['status'] == 'CANCELED'){
                    $order->stornirano = 1;
                }
            }

        	$order->save();


            //STOCK
            if(isset($row['items'])){
                foreach($row['items'] as $item){
                    $orderItem = null;
                    if(isset($item['id'])){
                        $orderItem = OrderB2CItem::where('web_b2c_narudzbina_stavka_id',$item['id'])->first();
                    }

                    if(is_null($orderItem)){
                        $orderItem = new OrderB2CItem();
                        $orderItem->web_b2c_narudzbina_id = $order->web_b2c_narudzbina_id;
                        $orderItem->broj_stavke = isset($item['sort']) ? $item['sort'] : null;
                        $orderItem->roba_id = $item['article_id'];
                        $orderItem->kolicina = $item['quantity'];
                        $orderItem->tarifna_grupa_id = $item['vat_id'];
                        $orderItem->racunska_cena_nc = isset($item['basic_price']) ? $item['basic_price'] : 0;
                        $orderItem->jm_cena = isset($item['unit_price']) ? $item['unit_price'] : 0;
                    }else{
                        if(isset($item['sort'])){
                            $orderItem->broj_stavke = $item['sort'];
                        }
                        if(isset($item['article_id'])){
                            $orderItem->roba_id = $item['article_id'];
                        }
                        if(isset($item['quantity'])){
                            $orderItem->kolicina = $item['quantity'];
                        }
                        if(isset($item['vat_id'])){
                            $orderItem->tarifna_grupa_id = $item['vat_id'];
                        }
                        if(isset($item['basic_price'])){
                            $orderItem->racunska_cena_nc = $item['basic_price'];
                        }
                        if(isset($item['unit_price'])){
                            $orderItem->jm_cena = $item['unit_price'];
                        }
                    }
                    
                    $orderItem->save();
                }
            }
        }

        return Response::json(['success'=>true],200);
	}

    public function b2cOrdersDelete(){
        $data = Input::get();
        $regex = AdminSupport::regex();

        if(count($data) > 20000){
            return Response::json(['success'=>false,'messages'=>'Max input vars is 20000.'],200);
        }

        if(!isset($data[0])){
            return Response::json(['success'=>false,'messages'=>'Array of inputs is required.'],200);
        }

        if(isset($data[0]['id'])){
            foreach($data as $row){
                $rules = array(
                    'id' => 'required|integer'
                );
                $validator = Validator::make($row,$rules);
                if($validator->fails()){
                    return Response::json(['success'=>false,'messages'=>$validator->messages()],200);
                    break;
                }
            }
        }
        else if(isset($data[0]['document_number'])){
            foreach($data as $row){
                $rules = array(
                    'document_number' => 'required|max:20'
                );
                $validator = Validator::make($row,$rules);
                if($validator->fails()){
                    return Response::json(['success'=>false,'messages'=>$validator->messages()],200);
                    break;
                }
            }
        }else{
            return Response::json(['success'=>false,'messages'=>'Element require id or document_number.'],200);
        }


        if(isset($data[0]['id'])){
            $ids = array_map(function($item){ return $item['id']; },$data);
            DB::table('web_b2c_narudzbina')->whereIn('web_b2c_narudzbina_id',$ids)->delete();
        }
        else if(isset($data[0]['document_number'])){
            $codes = array_map(function($item){ return $item['document_number']; },$data);
            DB::table('web_b2c_narudzbina')->whereIn('id_is',$codes)->delete();
        }

        return Response::json(['success'=>true],200);
    }


    public function b2cOrderItemsDelete(){
        $data = Input::get();
        $regex = AdminSupport::regex();

        if(count($data) > 20000){
            return Response::json(['success'=>false,'messages'=>'Max input vars is 20000.'],200);
        }

        if(!isset($data[0])){
            return Response::json(['success'=>false,'messages'=>'Array of inputs is required.'],200);
        }

        if(isset($data[0]['id'])){
            foreach($data as $row){
                $rules = array(
                    'id' => 'required|integer'
                );
                $validator = Validator::make($row,$rules);
                if($validator->fails()){
                    return Response::json(['success'=>false,'messages'=>$validator->messages()],200);
                    break;
                }
            }
        }else{
            return Response::json(['success'=>false,'messages'=>'Element require id or document_number.'],200);
        }


        if(isset($data[0]['id'])){
            $ids = array_map(function($item){ return $item['id']; },$data);
            DB::table('web_b2c_narudzbina_stavka')->whereIn('web_b2c_narudzbina_stavka_id',$ids)->delete();
        }

        return Response::json(['success'=>true],200);
    }

}