<!DOCTYPE html>
<html>
    <head>
        <title>{{$title}}</title>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="msapplication-tap-highlight" content="no"/> 

        <link rel="icon" type="image/png" href="{{ DokumentiOptions::base_url()}}favicon.ico">

        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
        <link href="{{ DokumentiOptions::base_url()}}css/foundation.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ DokumentiOptions::base_url()}}css/normalize.css" rel="stylesheet" type="text/css" />
        <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"> -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="{{ DokumentiOptions::base_url()}}css/alertify.core.css">
        <link rel="stylesheet" href="{{ DokumentiOptions::base_url()}}css/alertify.default.css">
        <link href="{{ DokumentiOptions::base_url()}}css/dokumenti/main.css" rel="stylesheet" type="text/css" />
        <script src="{{ DokumentiOptions::base_url()}}js/alertify.js" type="text/javascript"></script>
 
    </head>
<body>

    <div class="flex-wrapper">
       
        <!-- HEADER -->
        @include('dokumenti.partials.header')
        <!-- MAIN MENU -->
        @yield('content')
    </div> <!-- end of flex-wrapper -->


    <input type="hidden" id="base_url" value="{{DokumentiOptions::base_url()}}" />
    <input type="hidden" id="live_base_url" value="{{AdminOptions::live_base_url()}}" />
    <input type="hidden" id="isb2b" value="0" />
    <input type="hidden" id="sablon_change" value="{{ Session::has('sablon_change') ? Session::get('sablon_change') : 0 }}" />
    <input type="hidden" id="submit_order" value="{{ Session::has('submit_order') ? Session::get('submit_order') : 0 }}" />
    <script src="{{ DokumentiOptions::base_url()}}js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script src="{{ DokumentiOptions::base_url()}}js/jquery-ui.min.js"></script>
    <script src="{{ DokumentiOptions::base_url()}}js/foundation.min.js" type="text/javascript"></script>
    <script src="{{ DokumentiOptions::base_url()}}js/dokumenti/funkcije.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>
    <script src="{{ DokumentiOptions::base_url()}}js/dokumenti/main.js" type="text/javascript"></script>

    @if(in_array($strana,array('ponude','ponuda')))
    <script src="{{ DokumentiOptions::base_url()}}js/dokumenti/ponude.js" type="text/javascript"></script>
    @endif
    @if(in_array($strana,array('racuni','racun')))
    <script src="{{ DokumentiOptions::base_url()}}js/dokumenti/racuni.js" type="text/javascript"></script>
    @endif
    @if(in_array($strana,array('predracuni','predracun')))
    <script src="{{ DokumentiOptions::base_url()}}js/dokumenti/predracuni.js" type="text/javascript"></script>
    @endif

    @if(in_array($strana, array('podesavanja','ponuda','predracun')))
        <script type="text/javascript" src="{{ AdminOptions::base_url()}}js/tinymce_5.1.3/tinymce.min.js"></script>
        <script type="text/javascript">  
            tinymce.init({
                selector: ".special-textareas", 
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",    
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table paste",
                    "autoresize"
                ],       
                //IMAGE
                paste_data_images: true,  
                // Advanced in insert->media_poster
                media_poster: false,
                media_alt_source: false, 

                extended_valid_elements : "script[language|type|async|src|charset]",
                end_container_on_empty_block: true,
 
                contextmenu: "image", //  ------->  COPY PASTE
                toolbar: "insertfile undo redo | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | antrfile | glosa | forecolor backcolor | gallery",  
                // setup: function (editor) { 
                //     editor.ui.registry.addButton('antrfile', {
                //         text: 'Antrfile',
                //             onAction: function () {   
                //                 var text = editor.selection.getContent({'format': 'html'});
                //                 if(text && text.length > 0) {
                //                     editor.execCommand('mceInsertContent', false, '<div style="background-color: #141140; padding: 40px 20px; margin: 10px 0; color: #fff; display: inline-block;">'+ text +'</div>');
                //                 }
                //             }
                //     });
                //     editor.ui.registry.addButton('glosa', {
                //         text: 'Glosa',
                //             onAction: function () {   
                //                 var text = editor.selection.getContent({'format': 'html'});
                //                 if(text && text.length > 0) {
                //                     editor.execCommand('mceInsertContent', false, '<p style="text-align: center; font-style: italic; color:#a6a6a6; font-size: 22px; line-height: 1.3; margin: 22px 0;">"'+ text +'"</p>');
                //                 }
                //             }
                //     });
                //     if ($('.JSfixed_tinumce_toolbar')[0]) { 
                //         editor.ui.registry.addButton('gallery', {
                //             text: 'Gallery',
                //                 onAction: function () {
                //                     if($('#JSGalerijaModal').length > 0){
                //                         $('#JSGalerijaModal').foundation('reveal', 'open');
                //                     }
                //                 }
                //         });
                //     }
                // }
            });
        </script>
    @endif
    @if(in_array($strana,array('partneri')))
    <script type="text/javascript">
        function translate(string){
            return string;
        }
    </script>
    <script src="{{ AdminOptions::base_url()}}js/admin/admin_partneri.js" type="text/javascript"></script>
    @endif
<!--     @if(in_array($strana,array('test')))
        <link href="{{ DokumentiOptions::base_url()}}css/jquery.datetimepicker.min.css" rel="stylesheet">
        <script src="{{ DokumentiOptions::base_url()}}js/jquery.datetimepicker.full.min.js"></script>
    @endif -->

    @if(Session::has('message'))
        <script>
            alertify.success('{{ Session::get('message') }}');
        </script>
    @endif
    @if(Session::has('error_message'))
        <script>
            alertify.error('{{ Session::get('error_message') }}');
        </script>
    @endif
</body>
</html>