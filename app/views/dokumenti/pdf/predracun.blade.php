
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php /* header('Content-Type: text/html; charset=utf-8;'); */?> 
<title>{{ AdminLanguage::transAdmin('PDF') }}</title>
<style>

.row::after {content: ""; clear: both; display: table;}

[class*="col-"] { float: left; padding: 5px; display: inline-block;}

.col-1 {width: 8.33%;}
.col-2 {width: 16.66%;}
.col-3 {width: 25%;}
.col-4 {width: 33.33%;}
.col-5 {width: 41.66%;}
.col-6 {width: 50%;}
.col-7 {width: 58.33%;}
.col-8 {width: 66.66%;}
.col-9 {width: 75%;}
.col-10 {width: 83.33%;}
.col-11 {width: 91.66%;}
.col-12 {width: 100%;}

* {margin: 0; font-family: DejaVu Sans; box-sizing: border-box;}

body {font-size: 16px;}

table {border-collapse: collapse; font-size: 12px; margin-bottom: 15px; width: 100%;}

td, th {border: 1px solid #cacaca; text-align: left; padding: 3px;}

tr:nth-child(even) {background-color: #f5f6ff;}

h2 {margin-bottom: 15px;}

.container {width: 90%;	margin: 42px auto;}

.logo img {	max-width: 150px; width: 100%;}

.text-right {text-align: right;}

.signature {background: none; color: #808080;}

.signature td, th {border: none; background: none;}
 
.company-info {font-size: 11px;}

.comp-name {font-size: 18px; font-weight: bold;}
 
.kupac-info td {border: none;}

.kupac-info { border: 1px solid #ddd; }

thead {background: #eee;}

.ziro {font-weight: bold; font-size: 14px; margin: 10px 0;}
 
.napomena p {margin: 10px 0; font-size: 13px;}

.info-1 {margin-bottom: 30px;}

.rbr {width: 50px;}

.artikli td {text-align: center;}

.cell-product-name {text-align: left !important;}

.sum-span {margin-right: 20px;}

.page-break {
    page-break-after: always;
}

</style>
</head>
<body>
	 
	<div class="container">
	@foreach(range(0,$count_pages-1) as $page)
	<?php $stavke = DB::table('predracun_stavka')->where('predracun_id',$predracun->predracun_id)->limit($limit)->offset($page*$limit)->get(); ?>
		@if(!($page == ($count_pages-1) and !$show_header))
			@include('dokumenti.partials.pdf.predracun_header')
		@endif
		<div class="row"> 
		<table class="artikli">
			@if(!($page == ($count_pages-1) and !$show_header))
	        <tr>
	        	<td class="cell">{{ AdminLanguage::transAdmin('Rbr') }}</td>
	            <td class="cell-product-name">{{ AdminLanguage::transAdmin('Naziv proizvoda') }}</td>
	            <td class="cell">{{ AdminLanguage::transAdmin('Kol') }}.</td>
	            <td class="cell">{{ AdminLanguage::transAdmin('Cena') }}</td>
	            <td class="cell">{{ AdminLanguage::transAdmin('Poreska osnovica') }}</td>
	            <td class="cell">{{ AdminLanguage::transAdmin('PDV') }}</td>
	            <td class="cell">{{ AdminLanguage::transAdmin('Iznos PDV') }}</td>
	            <td class="cell">{{ AdminLanguage::transAdmin('Vrednost sa PDV') }}</td>
	        </tr>
	        @endif

	        @foreach($stavke as $row)
	        	<tr>
	        		<td class="cell"><?php echo $rbr; $rbr++; ?></td>
	                <td class="cell-product-name">{{ $row->naziv_stavke }}</td>
	                <td class="cell">{{ (int)$row->kolicina }}</td>
					@if(is_null($saradnik) || is_null($row->roba_id))
					<td class="cell">{{ AdminCommon::cena($row->nab_cena) }}</td>
					<td class="cell">{{ AdminCommon::cena($row->nab_cena * $row->kolicina) }}</td>
					<td class="cell">{{ $row->pdv }}</td>
					<td class="cell">{{ AdminCommon::cena($row->nab_cena * ($row->pdv/100) * $row->kolicina) }}</td>
					<td class="cell">{{ AdminCommon::cena($row->nab_cena * (1+$row->pdv/100) * $row->kolicina) }}</td>
					@else
					<?php $cena_sa_rabatom = $row->nab_cena * (1-B2bArticle::b2bRabatCene($row->roba_id)->ukupan_rabat/100); ?>
					<td class="cell">{{ AdminCommon::cena($cena_sa_rabatom) }}</td>
					<td class="cell">{{ AdminCommon::cena($cena_sa_rabatom * $row->kolicina) }}</td>
					<td class="cell">{{ $row->pdv }}</td>
					<td class="cell">{{ AdminCommon::cena($cena_sa_rabatom * ($row->pdv/100) * $row->kolicina) }}</td>
					<td class="cell">{{ AdminCommon::cena($cena_sa_rabatom * (1+$row->pdv/100) * $row->kolicina) }}</td>				
					@endif
	            </tr>
	        @endforeach
        </table>
        </div>
		@if($page == ($count_pages-1))
		    @include('dokumenti.partials.pdf.predracun_rekapitulacija')
			@include('dokumenti.partials.pdf.predracun_footer')
		@else
			<div class="page-break"></div>
			<br>
		@endif
	@endforeach

	</div>
 
</body>
</html>
