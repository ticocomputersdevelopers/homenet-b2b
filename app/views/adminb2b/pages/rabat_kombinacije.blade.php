@extends('adminb2b.defaultlayout')
@section('content')
 
<section class="" id="main-content">
	
	@include('adminb2b.partials.partner_rabat_tabs')

	<div class="row art-row">
		@if(Admin_model::check_admin(array('B2B_RABAT_AZURIRANJE')))
			<section class="medium-3 columns">
				<div class="flat-box">
					<h3 class='title-med'>Unos novih rabata</h3><br>
					<form class="rabat-combination-form" action="{{AdminOptions::base_url()}}admin/b2b/rabat_kombinacije" method="POST">
						<div class="select-control">
							<label></label>
							<select class="admin-select m-input-and-button__input" name="partner" id="partner_selector">
							<option value="-1">Izaberite partnera</option>
							@foreach($partneri as $row)
							<option value="{{ $row->partner_id }}">{{ $row->naziv }}</option>
							@endforeach
							</select>
						</div>
						<div class="select-control">
							<label></label>
							<select name="grupa" id="grupa_selector">
							{{ AdminB2BProizvodjac::getGroups(0) }}
							</select>
						</div>
						<div class="select-control">
							<label></label>
							<select name="proizvodjac" id="proizvodjac_selector">
							<option value="-1">Izaberite proizvođača</option>
							@foreach($proizvodjaci as $row)
							<option value="{{ $row->proizvodjac_id }}">{{ $row->naziv }}</option>
							@endforeach
							</select>
						</div>
						<div class="select-control"> 
							<label></label>
							<input type="number" step="0.01" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 || event.keyCode === 190 || event.keyCode === 37 || event.keyCode === 39 || event.keyCode === 109 ? true : !isNaN(Number(event.key))" name="rabat" required class="" placeholder="Rabat"> 
						</div>
						<div class="text-center">
							<input type="submit" class="btn-primary btn" value="Unesi">
						</div>
					</form>
				</div>
			</section>
		@endif
		<section class="large-8 medium-8 columns">
			<div id="comb-table" class="flat-box"> 
				<div class="columns medium-5 no-padd"> 
					<div class="btn-container"> 
						<input type="text" id="JSRebateCombinationsSearch" placeholder="Pretraga.." value="{{ $search }}"> 
					</div><br>
				</div> 
				<table class="articles-page-tab" id="myTable">
					<thead>
						<tr class="st order-list-titles row">
							<th class="table-head">Partner</th>
							<th class="table-head">Grupa</th>
							<th class="table-head">Proizvođač</th>
							<th class="table-head">Rabat</th>
							<th class="table-head"></th>
							</tr>
					</thead>
					<tbody class="proizvodjaci-b2b">
						@foreach($kombinacije as $kombinacija)
						<tr class="order-list-item row">
							<td>&nbsp; {{ $kombinacija->partner_naziv }}</td>
							<td>{{ $kombinacija->grupa }}</td>
							<td>{{ $kombinacija->proizvodjac_naziv }}</td>
							<td><input type="text" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 || event.keyCode === 190 || event.keyCode === 37 || event.keyCode === 39 || event.keyCode === 109 ? true : !isNaN(Number(event.key))" class="JSKombinacijaRabat" value="{{ $kombinacija->rabat }}" {{ Admin_model::check_admin(array('B2B_RABAT_AZURIRANJE')) == false ? 'readonly' : '' }}></td>
							<td>
								@if(Admin_model::check_admin(array('B2B_RABAT_AZURIRANJE')))
									<button class="JSUpdatePartnerRabatGrupa btn save-it-btn btn-small " data-partner_rabat_grupa_id="{{ $kombinacija->id }}">Sačuvaj</button>
									<button class="JSDeletePartnerRabatGrupa btn btn-small btn-danger" data-partner_rabat_grupa_id="{{ $kombinacija->id }}">Izbriši</button>
								@endif
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				{{ Paginator::make($kombinacije, $count_products, $limit)->links() }}
			</div>
		</section>
	</div>
</section>
@endsection

<!--   	methods: {
        save: function(id, rabat) {

        	$.post(
				base_url+'admin/b2b/ajax/rabat_edit', {
					action: 'rabat_kombinacija_edit',
					id: id,
					rabat: rabat
				}, function (response){
					if(response == 1) {
						alertify.success('Uspešno ste sačuvali rabat.');
					} else {
						alertify.error('Greška prilikom upisivanja u bazu.');
					}
				}
			);

            console.log("Saved ", id, rabat);
        },
        deleteRow: function(row, id) {

        	$.post(
				base_url+'admin/b2b/rabat_kombinacije/'+id+'/delete', {}, 
				function (response){
					if(response == 1) {
						alertify.success('Uspešno izbrisano');
						window.location.reload(true);

					} else {
						alertify.error('Greška');
					}
				}
			);

            //console.log("Deleted", id);
        }
    } -->


