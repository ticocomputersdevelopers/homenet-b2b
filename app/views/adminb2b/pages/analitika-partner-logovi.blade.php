@extends('adminb2b.defaultlayout')
@section('content')

<div id="main-content" class="kupci-page">
	
	<h3 class="title-med">{{ AdminLanguage::transAdmin('Logovi partnera') }}</h3>
 
	<div class="row article-edit-box">   
		<form method="GET" action="{{AdminOptions::base_url()}}admin/b2b/analitika/partner-logovi" class="columns medium-3"> 
			<div class="m-input-and-button">
				<input type="text" name="search" value="{{ urldecode($search) }}" placeholder="{{ AdminLanguage::transAdmin('Pretraga') }}..." class="m-input-and-button__input">
				<input class="btn btn-primary btn-small" value="Pretraga" type="submit">
				<a class="btn btn-danger btn-small" href="{{AdminOptions::base_url()}}admin/b2b/analitika/partner-logovi">{{ AdminLanguage::transAdmin('Poništi') }}</a>
			</div>  
		</form>
			

		<div class="columns medium-2"> 
			<select class="JSPartnerAnalitikaFilterSelect" data-name="partner_id">
				<option value="">Svi partneri</option>
				@foreach($partneri as $partner)
				<option value="{{ $partner->partner_id }}" {{ ($partner->partner_id==$partner_id) ? 'selected' : '' }}>{{ $partner->naziv }}</option>
				@endforeach
			</select>
		</div>

		<div class="columns medium-2"> 
			<select class="JSPartnerAnalitikaFilterSelect" data-name="log_b2b_akcija_id">
				<option value="">Svi logovi</option>
				@foreach($log_akcije as $log_akcija)
				<option value="{{ $log_akcija->log_b2b_akcija_id }}" {{ ($log_akcija->log_b2b_akcija_id==$log_b2b_akcija_id) ? 'selected' : '' }}>{{ $log_akcija->naziv }}</option>
				@endforeach
			</select>
		</div>

		<div class="columns medium-2"> 
			<select class="JSPartnerAnalitikaFilterSelect" data-name="log">
				<option value="">Sva logovanja</option>
				@foreach($logovanja as $logovanje)
				<option value="{{ strtotime($logovanje->start).'-'.strtotime($logovanje->finish) }}" {{ (strtotime($logovanje->start) == strtotime($datum_od)) ? 'selected' : '' }}>{{ $logovanje->naziv }} ({{ $logovanje->start }} - {{ $logovanje->finish }})</option>
				@endforeach
			</select>
		</div>

		<div class="columns medium-1">  
			<input id="datum_od_analitika" class="datum-val has-tooltip" name="datum_od_analitika" type="text" value="{{$od}}" placeholder="{{ AdminLanguage::transAdmin('Datum od') }}">
	 	</div>

	 	<div class="columns medium-1">  
			<input id="datum_do_analitika" class="datum-val has-tooltip" name="datum_do_analitika" type="text" value="{{$do}}" placeholder="{{ AdminLanguage::transAdmin('Datum do') }}"> 
		</div>

		<div class="columns medium-1">  
			<button id="JSExportAnalitikaExport" class="btn btn-primary btn-small">{{ AdminLanguage::transAdmin('Export') }}</button>
		</div>
	</div> 
 
 	<div class="row article-edit-box"> 
		<div class="columns medium-12 large-12">
			<label>{{ AdminLanguage::transAdmin('Ukupno') }}: {{ $count }}</label>
			<table>
				<tr>
					<th class="JSSort" data-sort_column="partner_id" data-sort_direction="{{ $sort_column == 'partner_id' ? ($sort_direction == 'asc' ? 'desc' : 'asc') : 'asc' }}">{{ AdminLanguage::transAdmin('ID') }}</th>
					<th class="JSSort" data-sort_column="partner_naziv" data-sort_direction="{{ $sort_column == 'partner_naziv' ? ($sort_direction == 'asc' ? 'desc' : 'asc') : 'asc' }}">{{ AdminLanguage::transAdmin('Partner') }}</th>
					<th class="JSSort" data-sort_column="log_naziv" data-sort_direction="{{ $sort_column == 'log_naziv' ? ($sort_direction == 'asc' ? 'desc' : 'asc') : 'asc' }}">{{ AdminLanguage::transAdmin('Log') }}</th>
					<th class="JSSort" data-sort_column="broj_logova" data-sort_direction="{{ $sort_column == 'broj_logova' ? ($sort_direction == 'asc' ? 'desc' : 'asc') : 'asc' }}">{{ AdminLanguage::transAdmin('Broj akcija') }}</th>
					<th>{{ AdminLanguage::transAdmin('Broj akcija po stavki') }}</th>
					<th>{{ AdminLanguage::transAdmin('Akcije') }}</th>
				</tr>
				@foreach($logovi as $row)
				<tr>
					<td style="width: 5%;">{{ $row->partner_id }}</td>
					<td style="width: 15%;">{{ $row->partner_naziv }}</td>
					<td style="width: 10%;">{{ $row->log_naziv }}</td>
					<td style="width: 5%;">{{ $row->broj_logova }}</td>
					<td style="width: 25%; vertical-align: top;">
						<span class="inline-block">
							<button class="JScollapse_btn btn btn-edit-simple">{{ AdminLanguage::transAdmin('Detaljnije') }}</button>
						</span>
						<table class="JS_collapse_content hide margin-top">
							@foreach(AdminB2BSupport::partner_b2b_logs_details($row->proizvodi) as $key => $proizvod)
							<tr>
								<td style="width: 90%;">{{ $proizvod->naziv }}</td>
								<td style="width: 10%;">{{ $proizvod->count }}</td>
							</tr>
							@endforeach
						</table>
					</td>
					<td style="width: 25%; vertical-align: top;">
						<span class="inline-block">
							<button class="JScollapse_btn btn btn-edit-simple">{{ AdminLanguage::transAdmin('Detaljnije') }}</button>
						</span>
						<table class="JS_collapse_content hide margin-top">
							<?php $proizvodi = !empty(json_decode($row->proizvodi)) ? json_decode($row->proizvodi) : []; ?>
							@foreach($proizvodi as $key => $proizvod)
							@if(($key+1)<50)
							<tr><td>{{ $proizvod->naziv }}</td><td>{{ isset($proizvod->datum) ? $proizvod->datum : '' }}</td></tr>
							@endif
							@endforeach
						</table>
					</td>
				</tr>
				@endforeach 
			</table>
			{{ Paginator::make($logovi,$count,$limit)->links() }}
		</div>
	</div> 
</div>
@endsection