<div class="product-list table-responsive" id="customTable">
    <table class="table table-bordered"> 
        <thead> 
            <tr> 
                <th>Na stanju</th>
                <th>Korpa</th>
                <th>Slika</th>
                <th>Naziv</th>
                <th>Količina</th>
                <th>Roba u dolasku</th>
                <th>Jedinica</th>
                <th>Broj komada u pakovanju</th>
                <th>Cena bez PDV-a</th>
                <th>Rabat</th>
                <th>Neto cena bez PDV-a</th>
                <th>PDV</th>
                <th>Neto cena sa PDV-om</th>
                <th>Detaljno</th>
            </tr>
        </thead>
        
        <tbody> 
            @foreach($query_products as $row)
                <?php
                $rabatCene = B2bArticle::b2bRabatCene($row->roba_id);
                $lager = B2bArticle::lagerObj($row->roba_id);
                $cartAmount = B2bBasket::getB2bQuantityItem($row->roba_id);
                $quantity = $lager->kolicina - ($lager->rezervisano + $cartAmount);
                $razlika = B2bArticle::price_diff($row->roba_id);               

                ?>

            <tr> 
                <td>
                    @if($quantity > 0)
                        <i class="fa fa-check" aria-hidden="true"></i>
                    @else 
                        <i class="fa fa-times">
                    @endif
                </td>
                <td class="table-btn">
                @if(B2bArticle::getStatusArticle($row->roba_id) == 1)
                    @if($quantity>0)

                    <div class="row" id="quantityCustom">
                        <div class="col-md-6 flexing quantity-change">
                        <a href="javascript:void(0)" class="JSProductListCartLess"><i class="fas fa-minus"></i></a>
                            <input class="JSProductListCartAmount add-amount" id="quantity-{{$row->roba_id}}" type="text" value="1">
                            <a href="javascript:void(0)" class="JSProductListCartMore"><i class="fas fa-plus"></i></a> 
                        </div>
                        <div class="col-md-4 no-padding">
                            <button class="button add-to-cart-products text-dark border-none" data-max-quantity="{{$quantity}}" data-roba-id="{{$row->roba_id}}"> 
                                <i class="fas fa-shopping-cart"></i>
                            </button>
                        </div>
                    </div>

                        <!-- <div class="inline-block quantity-change"> 
                            
                        </div> -->
                     
                        <!-- <button class="button add-to-cart-products" data-max-quantity="{{$quantity}}" data-roba-id="{{$row->roba_id}}"> 
                            U korpu
                        </button> -->
                        @if(!is_null(B2bPartner::dokumentiUser()))
                        <button class="button add-to-offer JSadd-to-offer-products hidden" data-roba-id="{{$row->roba_id}}">Dodaj u ponudu</button>
                        @endif
                    @else
                        <button class="button notAvailable">Nije dostupan</button>
                        @endif
                    @else
                    <button class="button">{{ B2bArticle::find_flag_cene(B2bArticle::getStatusArticle($row->roba_id),'naziv') }}</button>
                @endif  
                </td> 
                <td>
                <a href="{{B2bOptions::base_url()}}b2b/artikal/{{B2bUrl::slugify(B2bArticle::seo_title($row->roba_id))}}" class="product-image-wrapper">
                    <img class="img-responsive margin-auto" src="{{B2bOptions::base_url()}}{{ B2bArticle::web_slika($row->roba_id) }}" alt="{{ B2bArticle::seo_title($row->roba_id) }}" />
                </a>
                </td>
                <td class="relative">
                    <div class="flex customLabel">
                    <!-- Label -->
                    @if( Product::stiker_levo($row->roba_id) != null )
                        <a class="article-sticker-img">
                            <img src="{{ Options::domain() }}{{product::stiker_levo($row->roba_id) }}"  class="img-responsive logo" />
                        </a>
                    @endif 
                    
                    @if( Product::stiker_desno($row->roba_id) != null )
                        <a class="article-sticker-img text-right">
                            <img src="{{ Options::domain() }}{{product::stiker_desno($row->roba_id) }}" class="img-responsive logo"  />
                        </a>
                    @endif  

                    <!-- End of label -->
                    <a class="product-title" href="{{B2bOptions::base_url()}}b2b/artikal/{{B2bUrl::slugify(B2bArticle::seo_title($row->roba_id))}}"><span>{{ B2bArticle::short_title($row->roba_id) }}</span></a>
                    <div class="addon-title">{{ B2bArticle::addon_title($row->roba_id) }}</div>
                    @if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
                        @if(Session::has('b2c_admin'.B2bOptions::server()))
                        <a class="article-edit-btn" target="_blank" href="{{ B2bOptions::base_url() }}admin/product/{{ $row->roba_id }}">IZMENI ARTIKAL</a>
                        @endif
                    @endif

                    </div>
                </td>

                <td><span data-toggle="tooltip" title="Lager"> {{ number_format((int) $quantity,0,",",".") }} </span></td>


                @if(B2bArticle::get_roba_u_dolasku($row->roba_id,5) != 0)
                <td>
                    <button class="openmodal myBtn">{{ number_format((int) B2bArticle::get_roba_u_dolasku($row->roba_id,5),0,",",".") }}</button>
                        <!-- The Modal -->
                         <div class="modal myModal">
                          <!-- Modal content -->
                          <div class="modal-content">
                          <!-- <span class="close">&times;</span> -->
                            <div class="modal-header">
                                <h3 class="text-left my-2">Roba u dolasku</h3>
                                <div class="grid">
                                    <div>Datum</div>
                                    <div>Količina</div>
                                </div>
                                <div class="grid-2">
                                    <div><p>{{ B2bArticle::find_artical($row->roba_id,'model') }}</p></div>
                                    <div><p>{{ number_format((int) B2bArticle::get_roba_u_dolasku($row->roba_id,5),0,",",".") }}</p></div>
                                </div>                   
                            </div>

                            <hr>

                            <div class="modal-footer">
                               <button class="close">Zatvori</button>
                            </div>

                          <!-- <table>
                              <thead>
                                  <tr>
                                      <th>Datum</th>
                                      <th>Količina</th>
                                  </tr>
                              </thead>
                              <tbody>
                                 <tr>
                                     <td>{{ B2bArticle::find_artical($row->roba_id,'model') }}</td>
                                     <td>{{ number_format((int) B2bArticle::get_roba_u_dolasku($row->roba_id,5),0,",",".") }}</td>
                                 </tr> 
                              </tbody>
                          </table> -->
                          </div>
                        </div>
                </td>
                @else 
                <td>{{ 0 }}</td>
                @endif

                @if(B2bArticle::find_artical($row->roba_id,'flag_ambalaza') == 0)
                    <td>{{ B2bArticle::jedinica_mere_naziv($row->roba_id) }}</td>
                @else 
                    <td>Pakovanje</td>
                @endif

                @if(B2bArticle::find_artical($row->roba_id,'flag_ambalaza') == 1)
                    <td>{{  number_format((int) B2bArticle::find_artical($row->roba_id,'ambalaza'),0,",",".") }}</td>
                @else
                    <td>{{ 0 }}</td>
                @endif 
                <td>
                    @if(B2bArticle::getStatusArticle($row->roba_id) == 1)
                    <div data-toggle="tooltip" title="Neto cena"> {{B2bBasket::cena($rabatCene->osnovna_cena)}}</div>
                    @if($razlika > 0)<i class="price-change fa fa-arrow-up"></i>@elseif($razlika < 0)<i class="price-change fa fa-arrow-down">@endif</i>
                    @endif
                </td>

                <td>
                     
                    <div class="table-action">{{ B2bCommon::provera_akcija($row->roba_id) ? 'Akcija' : '' }} </div>
                    <span data-toggle="tooltip" title="Rabat">@if(B2bArticle::getStatusArticle($row->roba_id) == 1) {{number_format($rabatCene->ukupan_rabat,2)}} % @endif</span>
                </td>

                <td><span data-toggle="tooltip" title="Cena sa rabatom"> @if(B2bArticle::getStatusArticle($row->roba_id) == 1) {{B2bBasket::cena($rabatCene->cena_sa_rabatom)}} @endif</span></td>

                <td><span data-toggle="tooltip" title="PDV stopa"> @if(B2bArticle::getStatusArticle($row->roba_id) == 1){{number_format($rabatCene->porez,2)}} % @endif</span></td>

                <td><span data-toggle="tooltip" title="Cena sa PDV-om">@if(B2bArticle::getStatusArticle($row->roba_id) == 1) {{B2bBasket::cena($rabatCene->ukupna_cena,2)}} @endif</span></td>


                

 
                <td><a class="fa fa-arrow-circle-right fa-2x" aria-hidden="true" href="{{B2bOptions::base_url()}}b2b/artikal/{{B2bUrl::slugify(B2bArticle::seo_title($row->roba_id))}}"></a></td>
                
            </tr>
            @endforeach
        </tbody> 
    </table>

    <script type="text/javascript">
        var modals = document.getElementsByClassName('modal');
        // Get the button that opens the modal
        var btns = document.getElementsByClassName("openmodal");
        var spans=document.getElementsByClassName("close");
        for(let i=0;i<btns.length;i++){
           btns[i].onclick = function() {
              modals[i].style.display = "block";
           }
        }
        for(let i=0;i<spans.length;i++){
            spans[i].onclick = function() {
               modals[i].style.display = "none";
            }
         }
    </script>
</div>