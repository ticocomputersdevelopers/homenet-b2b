<header id="admin-header" class="row">
	<!-- WIDTH TOGGLE BUTTON -->
	<div class="header-width-toggle"><i class="fa fa-chevron-left" aria-hidden="true"></i></div>

	<div class="logo-wrapper">
		<a class="logo" href="{{ RmaOptions::base_url()}}admin" title="Selltico">
			<img src="{{ RmaOptions::base_url()}}/images/admin/logo-selltico-white.png" alt="Selltico">
		</a>
	</div>

	<!-- <span class="main-menu-toggler"></span> -->
	<nav class="main-menu">

		<ul class="clearfix">
			<li class="menu-item">
				<div class="">
					<div class="logged-user">Ulogovan: 
						@if($user = RmaOptions::user('admin'))
							{{ $user->ime.' '.$user->prezime }}
						@elseif($user = RmaOptions::user('servis'))
							{{ $user->naziv }}
						@endif
					</div>
				</div>
			</li>

			<li class="menu-item">
				<a href="{{RmaOptions::base_url()}}rma" class="menu-item__link  @if(in_array($strana,array('rma','radni_nalog_prijem','radni_nalog_opis_rada','radni_nalog_dokumenti','radni_nalog_predaja'))) active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-cubes" aria-hidden="true"></i>
						<span class="menu-tooltip">Radni nalozi</span>
					</div>
					<div class="menu-item__text">Radni nalozi</div>
				</a>
			</li>


			@if(RmaOptions::user('admin'))
			<li class="menu-item">
				<a href="{{RmaOptions::base_url()}}rma/operacije" class="menu-item__link  @if($strana == 'operacije') active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-file-text" aria-hidden="true"></i>
						<span class="menu-tooltip">Operacija</span>
					</div>
					<div class="menu-item__text">Operacija</div>
				</a>
			</li>
			@endif

			<li class="menu-item">
				<a href="{{RmaOptions::base_url()}}rma/serviseri" class="menu-item__link  @if(in_array($strana,array('serviseri'))) active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-users" aria-hidden="true"></i>
						<span class="menu-tooltip">Serviseri</span>
					</div>
					<div class="menu-item__text">Serviseri</div>
				</a>
			</li>
			
			@if(RmaOptions::user('admin'))
			<li class="menu-item">
				<a href="{{RmaOptions::base_url()}}admin" class="menu-item__link">
					<div class="menu-item__icon">
						B2C
						<span class="menu-tooltip">Admin prodavnice</span>
					</div>
					<div class="menu-item__text">Admin prodavnice</div>
				</a>
			</li>
			@endif

		</ul>
	</nav>

	<div class="logout-wrapper">
		<div class="menu-item">
			<a href="{{ RmaOptions::base_url()}}rma/logout" class="menu-item__link">
				<div class="menu-item__icon">
					<i class="fa fa-sign-out" aria-hidden="true"></i>
					<span class="menu-tooltip">Odjavi se</span>
				</div>
				<div class="menu-item__text">Odjavi se</div>
			</a>
		</div>
		<p class="footnote">TiCo &copy; {{ date('Y') }} - All rights reserved</p>
	</div>
</header>

