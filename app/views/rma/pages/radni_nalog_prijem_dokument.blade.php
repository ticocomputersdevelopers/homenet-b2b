<!DOCTYPE html>
<html><head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
<title>PDF</title>
<style>
* { 
	box-sizing: border-box;
 	padding: 0;
	margin: 0; 
	line-height: 1;	
} 
body {
	font-size: 14px;  
	font-family: DejaVu Sans;
}
ul { list-style-type: none; }

table {border-collapse: collapse; margin-bottom: 15px; width: 100%;}

table tr td, table tr th {
	border: 1px solid #cacaca; 
	text-align: left; 
	padding: 4px 5px;
	font-size: 12px;
}
 
.row::after {content: ""; clear: both; display: table;}
[class*="col-"] { float: left; padding: 4px 5px; display: inline-block;}

.col-1 {width: 8.33%;}
.col-2 {width: 16.66%;}
.col-3 {width: 25%;}
.col-4 {width: 33.33%;}
.col-5 {width: 41.66%;}
.col-6 {width: 50%;}
.col-7 {width: 58.33%;}
.col-8 {width: 66.66%;}
.col-9 {width: 75%;}
.col-10 {width: 83.33%;}
.col-11 {width: 91.66%;}
.col-12 {width: 100%;}
.inline-block { display: inline-block; }
.text-right{ text-align: right; }
.text-center{ text-align: center; } 
.relative { position: relative; }

.container{
	padding: 10px 20px; 
} 
.margin-btom{
	margin: 0 0 5px 0;
}
.text-uppercase{
	text-transform: uppercase;
}
h2{
	font-size: 13px;
}
p{
	padding: 0;
	margin: 0; 
	font-size: 12px;
	letter-spacing: -.1px;
}
.border-btom{ 
	border-bottom: 1px solid #000; 
} 
.box{ 
    width: 105px;
    height: 40px;
    border: 1px solid #000;
    right: 0;
    bottom: 0;
    padding: 23px 0;
    text-align: center;
    display: inline-block;
} 
/* .foo-2 p{
 	padding: 3px 0;
 }*/
 .foo-3 p,  .foo-4 p{
    max-width: 270px;
    margin: auto;
 } 
 
</style>
</head><body>
	<div class="container relative"> 
		<div class="row"> 
			<div class="col-5">
				<div>
					<h2 class="margin-btom">{{RmaOptions::company_name()}}</h2>
					<p>{{RmaOptions::company_adress()}}, {{RmaOptions::company_mesto()}}</p>
					<!-- <p>Centrala: {{RmaOptions::company_fax()}}</p> -->
					<!-- <p>Servis: {{RmaOptions::company_phone()}}</p> -->
					<!-- <p>PIB: {{RmaOptions::company_pib()}}</p>
					<p>E-mail: {{RmaOptions::company_email()}}</p> -->
				</div>
			</div>
			<div class="col-5"> 
 				<h2 class="text-uppercase margin-btom">Servisni nalog br. {{ $radni_nalog->broj_naloga }}</h2>
 				<p>Datum prijave kvara: <span class="border-btom">{{ RMA::format_datuma($radni_nalog->datum_prijema,true) }}</span></p> 
 				@if(!empty($radni_nalog->datum_servisiranja))
	  			<p>Datum izlaska na servis: <span class="border-btom">{{ RMA::format_datuma($radni_nalog->datum_servisiranja) }}</span></p>
	  			@else 
	  			<p>Datum izlaska na servis:  __________________</p> 
	  			@endif
			</div>
		</div>
  
	  	<div class="row">
	  		<div class="col-5"> 
		  		<h2 class="margin-btom">PODACI O PODNOSIOCU REKLAMACIJE</h2>
	  			<div class="row"> 
	  				<div class="col-3" style="width: 20%;"><p>Ime</p></div>	
	  				<div class="col-8"><p class="border-btom"> {{ $kupac->ime }}&nbsp;</p></div>	
	  			</div>

				<div class="row"> 
					<div class="col-3" style="width: 20%;"><p>Prezime</p></div>	
					<div class="col-8"><p class="border-btom"> {{ $kupac->prezime }}&nbsp;</p></div>	
				</div>

				<div class="row"> 
					<div class="col-3" style="width: 20%;"><p>Ulica i broj</p></div>	
					<div class="col-8"><p class="border-btom"> {{ $kupac->adresa }}&nbsp;</p></div>	
				</div>

				<div class="row"> 
					<div class="col-3" style="width: 20%;"><p>Mesto</p></div>	
					<div class="col-8"><p class="border-btom">{{ $kupac->mesto }}&nbsp;</p></div>	
				</div>

				<div class="row"> 
					<div class="col-3" style="width: 20%;"><p>Telefon</p></div>	
					<div class="col-8"><p class="border-btom">{{ $kupac->telefon }}&nbsp;</p></div>	
				</div>

				<!-- <div class="row"> 
					<div class="col-3"><p>Email</p></div>	
					<div class="col-8"><p class="border-btom">{{ $kupac->email }}&nbsp;</p></div>	
				</div>  -->
		  	</div>

		  	<div class="col-6" style="width: 55%;"> 
		  		<h2 class="margin-btom">PODACI O REKLAMIRANOM UREĐAJU</h2>
		  		<div class="row"> 
	  				<div class="col-5" style="width: 37%;"><p>Vrsta uređaja:</p></div>	
	  				<div class="col-6" style="width: 57%;"><p class="border-btom">&nbsp;</p></div>	
	  			</div>

	  			<div class="row"> 
	  				<div class="col-5" style="width: 37%;"><p>Model:</p></div>	
	  				<div class="col-6" style="width: 57%;"><p class="border-btom">{{ $radni_nalog->uredjaj }} &nbsp;</p></div>  	
	  			</div>

	  			<div class="row"> 
	  				<div class="col-5" style="width: 37%;"><p>Serijski broj:</p></div>	
	  				<div class="col-6" style="width: 57%;"><p class="border-btom">{{ $radni_nalog->serijski_broj }}&nbsp;</p></div>	
	  			</div>
   
   				<div class="row"> 
	  				<div class="col-5" style="width: 37%;"><p>Datum kupovine:</p></div>	
	  				<div class="col-6" style="width: 57%;"><p class="border-btom">{{ RMA::format_datuma($radni_nalog->datum_kupovine) }}&nbsp;</p></div>	
	  			</div>

   				<div class="row"> 
	  				<div class="col-5" style="width: 37%;"><p>Naziv maloprodaje:</p></div>	
	  				<div class="col-6" style="width: 57%;"><p class="border-btom">{{ $radni_nalog->maloprodaja }}&nbsp;</p></div>	
	  			</div>

	  			<div class="row"> 
	  				<div class="col-5" style="width: 37%;"><p>Broj fiskalnog računa (BI):</p></div>	
	  				<div class="col-6" style="width: 57%;"><p class="border-btom">{{ $radni_nalog->broj_fiskalnog_racuna }}&nbsp;</p></div>	
	  			</div> 
			</div>
		</div>

		<div class="row"><br>
			<div class="col-2" style="width: 10%;"> 
				<p>OPIS KVARA:</p>
			</div> 
			<div class="col-9" style="width: 86%;"> 
				<p class="border-btom">{{ $radni_nalog->opis_kvara }}</p>
			</div> 
		</div>

	 	<div class="row"><br>
	  		<div class="col-12 foo-2"> 
		  		<h2 class="margin-btom">PODACI O KVARU I IZVRŠENOJ INTERVENCIJI</h2>
		  		<div class="row"> 
		  			<div class="col-2" style="width: 11%;"> 
				  		<p>Uzrok kvara: </p>
				  	</div>
				  	<div class="col-9" style="width: 85%;"> 
				  		<p class="border-btom">{{ $radni_nalog->uzrok_kvara }}&nbsp;</p>
				  	</div>
				 </div>
				 <div class="row relative"> 
		  			<div class="col-3" style="width: 19%;"> 
				  		<p style="margin: 5px 0 0;">Opis izvršenih radova: </p> 
				  	</div>
				  	<div class="col-8" style="width: 77%; overflow: hidden;"> 
				  		<div class="border-btom" style="padding: 0 0 6px;">&nbsp;</div>
				  		<div class="border-btom" style="padding: 0 0 6px;">&nbsp;</div>
				  		<div class="border-btom" style="padding: 0 0 6px;">&nbsp;</div>
				  		<p style="position: absolute; top: 0; line-height: 20px;">{{ $radni_nalog->napomena_operacije }} &nbsp;</p>
				  	</div>
		  		</div>
<!-- 				<div class="row"> 
					<div class="col-12"> 
						<p class="border-btom">&nbsp;</p>
					</div>
				</div> -->
			</div>
		</div>

		<div class="row">
	  		<div class="col-8 foo-3">  
				<div class="row"> 
					<div class="col-3"><p>Trajanje popravke </p></div> 
					<div class="col-3"><p class="border-btom">&nbsp; </p></div> 
		  		</div>

		  		<div class="row"> 
					<div class="col-3"><p>Pređeni kilometri </p></div> 
					<div class="col-3"><p class="border-btom">&nbsp; </p></div> 
		  		</div>

		  		<div class="row"> 
					<div class="col-6"><p>Popravka u garantnom roku</p></div> 
					<div class="col-3"><p>DA&nbsp;&nbsp;&nbsp;&nbsp; NE </p></div> 
		  		</div>

 				<div class="row"> 
					<div class="col-6"><p>Broj garancijskih popravka (zaokružite)</p></div> 
					<div class="col-3"><p>1&nbsp;&nbsp;&nbsp; 2&nbsp;&nbsp;&nbsp; 3&nbsp;&nbsp;&nbsp; 4&nbsp;&nbsp;&nbsp; 5&nbsp;&nbsp;&nbsp;</p></div> 
		  		</div> 
			</div>

			<div class="col-3 text-right">
				<div class="box"> 
					<p>Mesto za <br>kupon</p>
				</div>
			</div>
		</div>
		<br>

		<div class="row">
	  		<div class="col-12">
	  			<h2 class="margin-btom">USLUGE, UPOTREBLJENI MATERIJAL I REZERVNI DELOVI ZA POPRAVKU</h2>
				<table>
					<tr>
						<th></th>
						<th class="text-center">ŠIFRA</th>
						<th class="text-center" style="width: 65%;">USLUGA / NAZIV MATERIJALA</th>
						<th class="text-center">KOLIČINA</th>
						<th class="text-center">CENA</th> 
					</tr>
					@foreach($radni_nalog_operacije as $op_key => $operacija)
					<tr>
						<th class="text-center">{{ ($op_key+1) }}</th>
						<td>{{ $operacija->operacija_id }}</td>
						<td>{{ $operacija->opis_operacije }}</td>
						<td>{{ $operacija->norma_sat }}</td>
						<td class="text-right">{{ RMA::format_cene($operacija->iznos) }}</td>
					</tr>
					@endforeach

					@foreach($radni_nalog_rezervni_delovi as $rd_key => $radni_nalog_rezervni_deo)
					<tr>
						<th class="text-center">{{ (count($radni_nalog_operacije)+$rd_key+1) }}</th>
						<td>{{ RMA::find($radni_nalog_rezervni_deo->roba_id,'sifra_is') }}</td>
						<td>{{ RMA::find($radni_nalog_rezervni_deo->roba_id,'naziv_web') }}</td>
						<td>{{ $radni_nalog_rezervni_deo->kolicina }}</td>
						@if(RmaOptions::gnrl_options(3043) == 1)
						<td class="text-right">{{ RMA::format_cene($radni_nalog_rezervni_deo->iznos) }}</td>
						@else
						<td></td>
						@endif
					</tr>
					@endforeach

					@if((count($radni_nalog_operacije)+count($radni_nalog_rezervni_delovi)) < 10)
						@foreach(range((count($radni_nalog_operacije)+count($radni_nalog_rezervni_delovi)+1),10) as $val)
						<tr>
							<th class="text-center">{{ $val }}</th>
							<th></th>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						@endforeach
					@endif
					<tr>
						<th></th>
						<td></td>
						<td></td>
						<td>Ukupno:</td>
						<td>{{ RMA::format_cene($cene->cena_rada) }}</td>
					</tr>

				</table>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-12">
				<!-- Napomena: {{ $radni_nalog->napomena }} -->
				<h2 class="margin-btom">ZA KUPCA</h2>  
				<div class="row">
					<div class="col-6">
						<p>Da li ste zadovoljni uslugom: &nbsp;&nbsp;&nbsp;&nbsp; DA&nbsp;&nbsp;&nbsp;&nbsp; NE </p>
					</div>
				 
					<div class="col-5">
						<p class="border-btom">Razlog</p>
					</div>
				</div> 
			</div>
		</div>

		<div class="row">
			<div class="col-11">
				 <p class="border-btom">&nbsp;</p>
			</div>
		</div>
	<!-- 	<div class="row">
			<div class="col-12">
				Datum: {{ RMA::format_datuma($radni_nalog->datum_zavrsetka,true) }}
			</div>
	 	</div> -->
	 	<div class="row text-center foo-4"><br> 
	 		<div class="col-5">
	 			<p class="border-btom">&nbsp;</p>
	 			<p>Potpis servisera</p>
	 		</div>
	 		<div class="col-5">
	 			<p class="border-btom">&nbsp;</p>
	 			<p>Potpis stranke</p>
	 		</div>
	 	</div>
	</div>  
</body></html>
