<div class="row">    
    <div class="quick-view-wrapper col-md-6 col-sm-6 col-xs-12">
        <img class="img-responsive margin-auto" src="{{ Options::domain().$image }}" alt="articlal-image">
        @if(All::provera_akcija($roba_id))
            <div class="label sale-label">  {{ Language::trans('Akcija') }} </div>
        @endif
    </div>

    <div class="col-md-6 col-sm-6 col-xs-12">
        <h2 class="article-heading" itemprop="name">{{ Product::seo_title($roba_id) }}</h2>
        <ul>    
           <!--  @if(!is_null($proizvodjac) AND $proizvodjac->proizvodjac_id > 0)
            <li>{{Language::trans('Proizvođač')}}: {{ Product::get_proizvodjac($roba_id) }}</li>
            @endif -->
            <!-- <li>{{ Product::get_karakteristike_short_grupe($roba_id) }}</li>  -->
            <li class="quick-view-price price-holder"> {{Language::trans('Cena')}}: {{ Cart::cena(Product::get_price($roba_id)) }} </li>
            @if(All::provera_akcija($roba_id))
            <li class=""><del> {{ Cart::cena(Product::old_price($roba_id)) }} </del></li> 
            @endif     
        </ul>
        <div class="add-to-cart-area"> 
            @if(AdminSupport::getStatusArticle($roba_id) == 1) 
                @if(Cart::check_avaliable($roba_id) > 0)

                <input type="number" name="kolicina" autocomplete="off" id="JSQuickViewAmount" class="modal-amount cart-amount" min="1" value="1">
                <button class="add-to-cart-artikal button JSAddToCartQuickView" data-roba_id="{{$roba_id}}">
                     {{ Language::trans('Dodaj u korpu') }}          
                </button>           
                @else    
                <button class="not-available button">{{Language::trans('Nije dostupno')}}</button>    
                @endif      
            @endif
          
            @if(Cart::kupac_id() > 0)
            <button class="like-it far fa-heart JSadd-to-wish" data-roba_id="{{$roba_id}}" title="{{ Language::trans('Dodaj na listu želja') }}"> 
            </button> 
            @else
            <button class="like-it far fa-heart JSnot_logged" data-roba_id="{{$roba_id}}" title="{{ Language::trans('Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima') }}"> 
            </button> 
            @endif                
    
        </div>
    </div>
</div>