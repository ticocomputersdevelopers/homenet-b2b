


<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="page-breadcrumb">
			<ul>
				<li>
					<a href="{{ Options::base_url() }}pocetna">
						Početna /
					</a>
				</li>
				<li>
					<a href="{{ Options::base_url() }}blog">
						Blog /
					</a>
				</li>
				<li>
					<a href="#!">
						{{ $naslov }}
					</a>
				</li>
			</ul>
		</div>

		<h2 class="page-heading">
			{{ $naslov }}
		</h2>
	</div>
</div>