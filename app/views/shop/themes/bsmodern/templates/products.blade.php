<!DOCTYPE html>
<html lang="sr">
<head>
	@include('shop/themes/'.Support::theme_path().'partials/head')

	<script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": "WebPage",
			"name": "<?php echo $title; ?>",
			"description": "<?php echo $description; ?>",
			"url" : "<?php echo Options::base_url().$url; ?>",
			"publisher": {
			"@type": "Organization",
			"name": "<?php echo Options::company_name(); ?>"
		}
	}
</script>
</head>
<body id="product-page" 
@if($strana == All::get_page_start()) id="start-page"  @endif 
@if(Support::getBGimg() != null) 
style="background-image: url({{Options::domain()}}{{Support::getBGimg()}}); background-size: cover; background-repeat: no-repeat; background-attachment: fixed; background-position: center;" 
@endif
>


@include('shop/themes/'.Support::theme_path().'partials/menu_top')
 
@include('shop/themes/'.Support::theme_path().'partials/header')

<!-- PRODUCTS.blade -->
<main class="d-content JSmain relative"> 
			@if((!empty($baner_id) AND $banner = Slider::slajder($baner_id) AND count($bannerStavke = Slider::slajderStavke($banner->slajder_id)) > 0) AND !empty($bannerStavka = $bannerStavke[0]))
				<!-- BANNERS SECTION -->
				<div class="row relative">
					<div class="col-md-12 col-sm-12 col-xs-12 no-padding">
						<a class="center-block" href="{{ $bannerStavka->link }}">
							<img class="img-responsive margin-auto" src="{{ Options::domain() }}{{ $bannerStavka->image_path }}" alt="{{$bannerStavka->alt}}" />
						</a>

						@if($bannerStavka->sadrzaj != '')
						<div class="sliderText short-desc JSInlineFull content hidden-xs" data-target='{"action":"slide_content","id":"{{$bannerStavka->slajder_stavka_id}}"}'>
							{{ $bannerStavka->sadrzaj }}
						</div>
						@endif
					</div>
				
					<ul class="breadcrumbBanner breadcrumb">
						@if($strana!='pretraga' and $strana!='filteri' and $strana!='proizvodjac')
						{{ Url_mod::breadcrumbs2()}}
						@elseif($strana == 'proizvodjac')
						<li>
							<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('brendovi') }}">{{ Language::trans('Brendovi') }}</a>  
							@if($grupa)
							<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('proizvodjac') }}/{{ Url_mod::slug_trans($proizvodjac) }}">{{ All::get_manofacture_name($proizvodjac_id) }}</a>
							{{ Groups::get_grupa_title($grupa) }}
							@else
							{{ All::get_manofacture_name($proizvodjac_id) }} 
							@endif
						</li>
						@else					
						<li><a href="{{ Options::base_url() }}">{{ All::get_title_page_start() }}</a></li>
						<li>{{$title}}</li>
						@endif
					</ul>
					

				</div>
				<!-- Banners Section End -->

				<div class="productPageFiltersMobile">
				<!-- Filter Mobile -->
				<ul class="filterMobileContent hidden-lg hidden-md col-xs-12">
					<li class="relative">
						<a href="javascript:void(0)">{{ Language::trans('Filteri') }}</a> <i class="fas fa-chevron-down"></i>
					</li>
				</ul>
				<!-- End of Filter Mobile -->

				<ul class="selected-filters">
					<?php $br=0;
					foreach($niz_proiz as $row){
						$br+=1;
						if($row>0){
							?>
							<li>
								{{All::get_manofacture_name($row)}}     
								<a href="javascript:void(0)" rel=”nofollow”>
									<span class="JSfilter-close fas fa-times" data-type="1" data-rb="{{$br}}" data-element="{{$row}}"></span>
								</a>
							</li>
					<?php }}

					$br=0;
					foreach($niz_karakteristike as $row){
					$br+=1;
					if($row>0){
						?>
						<li>
							{{All::get_fitures_name($row)}}
							<a href="javascript:void(0)" rel=”nofollow”>
								<span class="JSfilter-close fas fa-times" data-type="2" data-rb="{{$br}}" data-element="{{$row}}"></span>
							</a>
						</li>
					<?php }}
					if($strana == 'pretraga'){
					foreach($niz_grupa as $row){
					$br+=1;
					if($row>0){
						?>
						<li>
							{{Groups::getGrupa($row)}}
							<a href="javascript:void(0)" rel=”nofollow”>
								<span class="JSfilter-close fas fa-times" data-type="2" data-rb="{{$br}}" data-element="{{$row}}"></span>
							</a>
						</li>
					<?php }}}?>
				</ul>
					<input type="hidden" id="JSmanufacturer_ids" value="{{$proizvodjac_ids}}"/>
					<input type="hidden" id="JSmanufacturer_attr" value="{{$kara}}"/>	
					@if($strana == 'pretraga')	
					<input type="hidden" id="JSGroups_ids" value="{{$grupa_ids}}"/>
					@endif

			</div>	 
				<ul class="mobileFilter hidden-lg hidden-md hidden" id="mobileFilterMobileNone">   
					@if(count($manufacturers)>0)
					<li class="manufacturerOpen">
						<a href="javascript:void(0)" class="center-block" rel=”nofollow”>{{ Language::trans('Proizvođač') }}
							<i class="fas fa-angle-down pull-right"></i>
						</a>

						<div class="JSfilters-slide-toggle-content filterSubmenu">
							@foreach($manufacturers as $key => $value)
							@if(in_array($key,$niz_proiz))
								<label>
									<input name="proizvodjac[]" onchange="filters_send()" value="{{ $key }}" type="checkbox" checked>
									<span class="filter-text center-block">
										{{All::get_manofacture_name($key)}} 
									</span>
									<span class="filter-count">
										@if(Options::filters_type()) {{ $value }} @endif
									</span>
								</label>
							@else
								<label>
									<input name="proizvodjac[]" onchange="filters_send()" value="{{ $key }}" type="checkbox">
									<span class="filter-text center-block">
										{{All::get_manofacture_name($key)}} 
									</span>
									<span class="filter-count">
										@if(Options::filters_type()) {{ $value }} @endif
									</span>
								</label>
							@endif
							@endforeach
						</div>				 				 
					</li>
					@endif				
					@if($characteristics > 0)
						@foreach($characteristics as $keys => $values)				
						<li>
							<a href="javascript:void(0)" class=" center-block" rel=”nofollow”>
								{{Language::trans($keys)}} 
								<i class="fas fa-angle-down pull-right"></i>  							
							</a>

							<div class="JSfilters-slide-toggle-content openFilterSubmenu">
								<div class="filterSubmenuDown">
								@foreach($values as $key => $value)
									<?php if(in_array($key, $niz_karakteristike)){ ?>
										<label>
											<input type="checkbox" onchange="filters_send()" name="vrednost[]" value="{{ $key }}" checked>
											<span class="filter-text center-block">
												{{All::get_fitures_name($key)}}
											</span>
											<span class="filter-count">
												@if(Options::filters_type()) {{ $value }} @endif
											</span>
										</label>
									<?php }else {?>
										<label>
											<input type="checkbox" onchange="filters_send()" name="vrednost[]" value="{{ $key }}"> 
											<span class="filter-text center-block">
												{{All::get_fitures_name($key)}}
											</span>
											<span class="filter-count">
												@if(Options::filters_type()) {{ $value }} @endif
											</span>
										</label>
									<?php } ?>
								@endforeach
								</div>
							</div>
						</li>
						@endforeach	
					@endif		
				</ul>	 

				<input type="hidden" id="JSfilters-url" value="{{$url}}" />
				</div>

				<!-- End of Filter Mobile -->

				
				<!-- Filter Drop Desktop Version -->
					<div class="filter-drop container hidden-sm hidden-xs">

						<div>  
							<div class="clearfix JShidden-if-no-filters">
								<span>{{ Language::trans('Izabrani filteri') }}:</span>
								<a class="JSreset-filters-button inline-block pull-right" role="button" href="javascript:void(0)" rel=”nofollow”>{{ Language::trans('Poništi filtere') }} <span class="fas fa-times"></span></a>
							</div> 

							<ul class="selected-filters">
								<?php $br=0;
								foreach($niz_proiz as $row){
									$br+=1;
									if($row>0){
										?>
										<li>
											{{All::get_manofacture_name($row)}}     
											<a href="javascript:void(0)" rel=”nofollow”>
												<span class="JSfilter-close fas fa-times" data-type="1" data-rb="{{$br}}" data-element="{{$row}}"></span>
											</a>
										</li>
								<?php }}

								$br=0;
								foreach($niz_karakteristike as $row){
								$br+=1;
								if($row>0){
									?>
									<li>
										{{All::get_fitures_name($row)}}
										<a href="javascript:void(0)" rel=”nofollow”>
											<span class="JSfilter-close fas fa-times" data-type="2" data-rb="{{$br}}" data-element="{{$row}}"></span>
										</a>
									</li>
								<?php }}
								if($strana == 'pretraga'){
								foreach($niz_grupa as $row){
								$br+=1;
								if($row>0){
									?>
									<li>
										{{Groups::getGrupa($row)}}
										<a href="javascript:void(0)" rel=”nofollow”>
											<span class="JSfilter-close fas fa-times" data-type="2" data-rb="{{$br}}" data-element="{{$row}}"></span>
										</a>
									</li>
								<?php }}}?>
							</ul>
							<input type="hidden" id="JSmanufacturer_ids" value="{{$proizvodjac_ids}}"/>
							<input type="hidden" id="JSmanufacturer_attr" value="{{$kara}}"/>	
							@if($strana == 'pretraga')	
							<input type="hidden" id="JSGroups_ids" value="{{$grupa_ids}}"/>
							@endif

						</div>	 


							<ul>   
								<div class="row container margin-auto">
									<div class="col-md-2 flexing-group">
										@if(count($manufacturers)>0)
										<li>
											<a href="javascript:void(0)" class="filterCustomLinks center-block px-5" rel=”nofollow”>{{ Language::trans('Proizvođač') }}
												<i class="fas fa-angle-down pull-right"></i>
											</a>

											<div class="filterOpener">
												@foreach($manufacturers as $key => $value)
												<div class="filterSubmenuFirst">

												
												@if(in_array($key,$niz_proiz))
													<label class="flex">
														<input name="proizvodjac[]" onchange="filters_send()" value="{{ $key }}" type="checkbox" checked>
														<span class="filter-text center-block">
															{{All::get_manofacture_name($key)}} 
														</span>
														<span class="filter-count">
															@if(Options::filters_type()) {{ $value }} @endif
														</span>
													</label>
												@else
													<label class="flex">
														<input name="proizvodjac[]" onchange="filters_send()" value="{{ $key }}" type="checkbox">
														<span class="filter-text center-block">
															{{All::get_manofacture_name($key)}} 
														</span>
														<span class="filter-count">
															@if(Options::filters_type()) {{ $value }} @endif
														</span>
													</label>
												@endif
												@endforeach
												</div>
											</div>				 				 
										</li>
										@endif				
									@if($characteristics > 0)
										@foreach($characteristics as $keys => $values)				
										<li class="relative openSubMenu">
											<a href="javascript:void(0)" class="filterCustomLinks center-block" rel=”nofollow”>
												{{Language::trans($keys)}} 
												<i class="fas fa-angle-down pull-right"></i>  							
											</a>

											<div class="filterOpener">
												<div class="filterSubmenuDown">

												
												@foreach($values as $key => $value)
													<?php if(in_array($key, $niz_karakteristike)){ ?>
														<label>
															<input type="checkbox" onchange="filters_send()" name="vrednost[]" value="{{ $key }}" checked>
															<span class="filter-text center-block">
																{{All::get_fitures_name($key)}}
															</span>
															<span class="filter-count">
																@if(Options::filters_type()) {{ $value }} @endif
															</span>
														</label>
													<?php }else {?>
														<label>
															<input type="checkbox" onchange="filters_send()" name="vrednost[]" value="{{ $key }}"> 
															<span class="filter-text center-block">
																{{All::get_fitures_name($key)}}
															</span>
															<span class="filter-count">
																@if(Options::filters_type()) {{ $value }} @endif
															</span>
														</label>
													<?php } ?>
												@endforeach
											</div>
											</div>
										</li>
										@endforeach	
									@endif		
								 </ul>	
							 </div>
						<!-- col  -->
						</div>
					</div>
					<!-- End of Filter Drop -->
				@else		
			@endif



			<div class="filterMobileArticles hidden-lg hidden-md">
				<!-- Filter Mobile -->
				<ul class="filterMobileContent hidden-lg hidden-md col-xs-12">
					<li class="relative">
						<a href="javascript:void(0)">{{ Language::trans('Filteri') }}</a> <i class="fas fa-chevron-down"></i>
					</li>
				</ul>
				<!-- End of Filter Mobile -->

			<ul class="selected-filters hidden-lg hidden-md">
				<?php $br=0;
				foreach($niz_proiz as $row){
					$br+=1;
					if($row>0){
						?>
						<li>
							{{All::get_manofacture_name($row)}}     
							<a href="javascript:void(0)" rel=”nofollow”>
								<span class="JSfilter-close fas fa-times" data-type="1" data-rb="{{$br}}" data-element="{{$row}}"></span>
							</a>
						</li>
				<?php }}

				$br=0;
				foreach($niz_karakteristike as $row){
				$br+=1;
				if($row>0){
					?>
					<li>
						{{All::get_fitures_name($row)}}
						<a href="javascript:void(0)" rel=”nofollow”>
							<span class="JSfilter-close fas fa-times" data-type="2" data-rb="{{$br}}" data-element="{{$row}}"></span>
						</a>
					</li>
				<?php }}
				if($strana == 'pretraga'){
				foreach($niz_grupa as $row){
				$br+=1;
				if($row>0){
					?>
					<li>
						{{Groups::getGrupa($row)}}
						<a href="javascript:void(0)" rel=”nofollow”>
							<span class="JSfilter-close fas fa-times" data-type="2" data-rb="{{$br}}" data-element="{{$row}}"></span>
						</a>
					</li>
				<?php }}}?>
			</ul>
				<input type="hidden" id="JSmanufacturer_ids" value="{{$proizvodjac_ids}}"/>
				<input type="hidden" id="JSmanufacturer_attr" value="{{$kara}}"/>	
				@if($strana == 'pretraga')	
				<input type="hidden" id="JSGroups_ids" value="{{$grupa_ids}}"/>
				@endif

		</div>	 
		<ul class="mobileFilter hidden-lg hidden-md hidden">   
			@if(count($manufacturers)>0)
			<li class="manufacturerOpen">
				<a href="javascript:void(0)" class="center-block" rel=”nofollow”>{{ Language::trans('Proizvođač') }}
					<i class="fas fa-angle-down pull-right"></i>
				</a>

				<div class="JSfilters-slide-toggle-content filterSubmenu">
					@foreach($manufacturers as $key => $value)
					@if(in_array($key,$niz_proiz))
						<label>
							<input name="proizvodjac[]" onchange="filters_send()" value="{{ $key }}" type="checkbox" checked>
							<span class="filter-text center-block">
								{{All::get_manofacture_name($key)}} 
							</span>
							<span class="filter-count">
								@if(Options::filters_type()) {{ $value }} @endif
							</span>
						</label>
					@else
						<label>
							<input name="proizvodjac[]" onchange="filters_send()" value="{{ $key }}" type="checkbox">
							<span class="filter-text center-block">
								{{All::get_manofacture_name($key)}} 
							</span>
							<span class="filter-count">
								@if(Options::filters_type()) {{ $value }} @endif
							</span>
						</label>
					@endif
					@endforeach
				</div>				 				 
			</li>
			@endif				
			@if($characteristics > 0)
				@foreach($characteristics as $keys => $values)				
				<li>
					<a href="javascript:void(0)" class=" center-block" rel=”nofollow”>
						{{Language::trans($keys)}} 
						<i class="fas fa-angle-down pull-right"></i>  							
					</a>

					<div class="JSfilters-slide-toggle-content openFilterSubmenu">
						<div class="filterSubmenuDown">
						@foreach($values as $key => $value)
							<?php if(in_array($key, $niz_karakteristike)){ ?>
								<label>
									<input type="checkbox" onchange="filters_send()" name="vrednost[]" value="{{ $key }}" checked>
									<span class="filter-text center-block">
										{{All::get_fitures_name($key)}}
									</span>
									<span class="filter-count">
										@if(Options::filters_type()) {{ $value }} @endif
									</span>
								</label>
							<?php }else {?>
								<label>
									<input type="checkbox" onchange="filters_send()" name="vrednost[]" value="{{ $key }}"> 
									<span class="filter-text center-block">
										{{All::get_fitures_name($key)}}
									</span>
									<span class="filter-count">
										@if(Options::filters_type()) {{ $value }} @endif
									</span>
								</label>
							<?php } ?>
						@endforeach
						</div>
					</div>
				</li>
				@endforeach	
			@endif		
		</ul>	 

		<input type="hidden" id="JSfilters-url" value="{{$url}}" />
		</div>

		<!-- End of Filter Mobile -->												



	<div class="container"> 							
		<div class="row mb-1">  		
			@if($strana!='akcija' and $strana!='tip')
			<ul class="breadcrumb">
				@if($strana!='pretraga' and $strana!='filteri' and $strana!='proizvodjac')
				{{ Url_mod::breadcrumbs2()}}
				@elseif($strana == 'proizvodjac')
				<li>
					<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('brendovi') }}">{{ Language::trans('Brendovi') }}</a>  
					@if($grupa)
					<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('proizvodjac') }}/{{ Url_mod::slug_trans($proizvodjac) }}">{{ All::get_manofacture_name($proizvodjac_id) }}</a>
					{{ Groups::get_grupa_title($grupa) }}
					@else
					{{ All::get_manofacture_name($proizvodjac_id) }} 
					@endif
				</li>
				@else					
				<li><a href="{{ Options::base_url() }}">{{ All::get_title_page_start() }}</a></li>
				<li>{{$title}}</li>
				@endif
			</ul>
			@endif   

			<div class="col-md-3 col-sm-12 col-xs-12">  

				@if($filter_prikazi == 0)
					@if($strana == 'proizvodjac')
						@include('shop/themes/'.Support::theme_path().'partials/categories/manufacturer_category')
					@elseif($strana == 'tip')
						@include('shop/themes/'.Support::theme_path().'partials/categories/tip_category')
					@elseif($strana == 'akcija')
						@include('shop/themes/'.Support::theme_path().'partials/categories/akcija_category')
					@endif
				@endif

				@if(Options::enable_filters()==1 AND $filter_prikazi)
					@include('shop/themes/'.Support::theme_path().'partials/products/filters')
				@endif

				<div class="hiddne-sm hidden-xs">&nbsp; <!-- EMPTY SPACE --></div>

				<?php $query_category_first=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>0,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>

				<!-- Mobile Version -->
				<div class="mobileDivCategories hidden-md hidden-lg">
					<ul class="sideCategoriesMobile">
						<li>
						<a href="javascript:void(0)">{{ Language::trans('Sve kategorije') }}</a> <i class="fas fa-chevron-down"></i>
						</li>
					</ul>

				<ul class="customLevel-1 sideCategories hidden">
					@if(Options::category_type()==1) 
					@foreach ($query_category_first as $row1)
					@if(Groups::broj_cerki($row1->grupa_pr_id) >0)

				<li>
					<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}" class="">
						@if(Groups::check_image_grupa($row1->putanja_slika))
						<span class="lvl-1-img-cont inline-block text-center hidden-sm hidden-xs hidden"> 
							<img src="{{ Options::domain() }}{{$row1->putanja_slika}}" alt="{{ $row1->grupa }}" />
						</span>
						@endif
						{{ Language::trans($row1->grupa)  }} 
					</a>

					<?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc') ?>

					<ul class="customLevel-2 row">
						@foreach ($query_category_second->get() as $row2)
						<li class="col-md-12 col-sm-12 col-xs-12">  
						{{ Language::trans($row2->grupa) }}

							<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}">
								{{ Language::trans($row2->grupa) }}

								@if(Groups::check_image_grupa($row2->putanja_slika))
								<span class="lvl-2-img-cont inline-block hidden-sm hidden-xs hidden">
									<img src="{{ Options::domain() }}{{$row2->putanja_slika}}" alt="{{ $row2->grupa }}" />
								</span>
								@endif
								
								<!-- {{ Language::trans($row2->grupa) }} -->
								
							</a>
			
							@if(Groups::broj_cerki($row2->grupa_pr_id) >0)
							<?php $query_category_third=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row2->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
							
							<ul class="JSlevel-3 customLevel-3">
								@foreach($query_category_third as $row3)
								
								<li>						 
									<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}/{{ Url_mod::slug_trans($row3->grupa) }}">{{ Language::trans($row3->grupa) }}</a>

									<!-- @if(Groups::broj_cerki($row3->grupa_pr_id) >0)
									<span class="JSCategoryLinkExpend hidden-sm hidden-xs">
										<i class="fas fa-chevron-down" aria-hidden="true"></i>
									</span>
									@endif   -->

									@if(Groups::broj_cerki($row3->grupa_pr_id) >0)
									<?php $query_category_forth=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row3->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>

									<ul class="JSlevel-4">
										@foreach($query_category_forth as $row4)
										<li>
											<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}/{{ Url_mod::slug_trans($row3->grupa) }}/{{ Url_mod::slug_trans($row4->grupa) }}">{{ Language::trans($row4->grupa) }}</a>
										</li>
										@endforeach
									</ul>
									@endif		
								</li>					 	
								@endforeach
							</ul>
							@endif
						</li>
						@endforeach
					</ul>
				</li>

				@else

				<li>		 
					<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}">		 
						@if(Groups::check_image_grupa($row1->putanja_slika))
						<span class="lvl-1-img-cont inline-block text-center hidden-sm hidden-xs"> 
							<img src="{{ Options::domain() }}{{$row1->putanja_slika}}" alt="{{ $row1->grupa }}" />
						</span>
						@endif
						{{ Language::trans($row1->grupa)  }}  				
					</a>			 
				</li>
				@endif
				@endforeach
				@else
				@foreach ($query_category_first as $row1)
				

				@if(Groups::broj_cerki($row1->grupa_pr_id) >0)
				<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}">{{ Language::trans($row1->grupa)  }}</a>
				<?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc') ?>


				@foreach ($query_category_second->get() as $row2)
				<li>
					<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}">
						{{ Language::trans($row2->grupa) }}
					</a>
					@if(Groups::broj_cerki($row2->grupa_pr_id) >0)
					<?php $query_category_third=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row2->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
					<ul>
						@foreach($query_category_third as $row3)
						<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}/{{ Url_mod::slug_trans($row3->grupa) }}">{{ Language::trans($row3->grupa) }}
						</a>
						@if(Groups::broj_cerki($row3->grupa_pr_id) >0)
						<?php $query_category_forth=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row3->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>

						<ul>
							@foreach($query_category_forth as $row4)
							<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}/{{ Url_mod::slug_trans($row3->grupa) }}/{{ Url_mod::slug_trans($row4->grupa) }}">{{ Language::trans($row4->grupa) }}</a>
							@endforeach
						</ul>
						@endif	
						@endforeach
					</ul>
					@endif
				</li>
				@endforeach

				@else
					<li>
						<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}">{{ Language::trans($row1->grupa) }}</a>
					</li>
				@endif 
				@endforeach				
				@endif

				@if(Options::all_category()==1)
				<li>
					<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sve-kategorije') }}">{{ Language::trans('Sve kategorije') }}</a>
				</li>
				@endif

			</ul>
		</div>
			<!-- End of mobile version -->
										

			 <ul class="customLevel-1 sideCategories hidden-sm hidden-xs">
				@if(Options::category_type()==1) 
				@foreach ($query_category_first as $row1)
				@if(Groups::broj_cerki($row1->grupa_pr_id) >0)

			 <li>
				<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}" class="">
					@if(Groups::check_image_grupa($row1->putanja_slika))
					<span class="lvl-1-img-cont inline-block text-center hidden-sm hidden-xs hidden"> 
						<img src="{{ Options::domain() }}{{$row1->putanja_slika}}" alt="{{ $row1->grupa }}" />
					</span>
					@endif
					{{ Language::trans($row1->grupa)  }} 
				</a>

				 <?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc') ?>

				<ul class="customLevel-2 row">
					@foreach ($query_category_second->get() as $row2)
					<li class="col-md-12 col-sm-12 col-xs-12">  

						<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}">
					{{ Language::trans($row2->grupa) }}

							@if(Groups::check_image_grupa($row2->putanja_slika))
							<span class="lvl-2-img-cont inline-block hidden-sm hidden-xs hidden">
								<img src="{{ Options::domain() }}{{$row2->putanja_slika}}" alt="{{ $row2->grupa }}" />
							</span>
							@endif
							
							<!-- {{ Language::trans($row2->grupa) }} -->
							
						</a>
		
						@if(Groups::broj_cerki($row2->grupa_pr_id) >0)
						<?php $query_category_third=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row2->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
						
						<ul class="JSlevel-3 customLevel-3">
							@foreach($query_category_third as $row3)
							
							<li>						 
								<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}/{{ Url_mod::slug_trans($row3->grupa) }}">{{ Language::trans($row3->grupa) }}</a>

								<!-- @if(Groups::broj_cerki($row3->grupa_pr_id) >0)
								<span class="JSCategoryLinkExpend hidden-sm hidden-xs">
									<i class="fas fa-chevron-down" aria-hidden="true"></i>
								</span>
								@endif   -->

								@if(Groups::broj_cerki($row3->grupa_pr_id) >0)
								<?php $query_category_forth=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row3->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>

								<ul class="JSlevel-4">
									@foreach($query_category_forth as $row4)
									<li>
										<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}/{{ Url_mod::slug_trans($row3->grupa) }}/{{ Url_mod::slug_trans($row4->grupa) }}">{{ Language::trans($row4->grupa) }}</a>
									</li>
									@endforeach
								</ul>
								@endif		
							</li>					 	
							@endforeach
						</ul>
						@endif
					</li>
					@endforeach
				</ul>
			</li>

			@else

			<li>		 
				<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}">		 
					@if(Groups::check_image_grupa($row1->putanja_slika))
					<span class="lvl-1-img-cont inline-block text-center hidden-sm hidden-xs"> 
						<img src="{{ Options::domain() }}{{$row1->putanja_slika}}" alt="{{ $row1->grupa }}" />
					</span>
					@endif
					{{ Language::trans($row1->grupa)  }}  				
				</a>			 
			</li>
			@endif
			@endforeach
			@else
			@foreach ($query_category_first as $row1)
			

			@if(Groups::broj_cerki($row1->grupa_pr_id) >0)
			<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}">{{ Language::trans($row1->grupa)  }}</a>
			<?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc') ?>


			@foreach ($query_category_second->get() as $row2)
			<li>
				<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}">
					{{ Language::trans($row2->grupa) }}
				</a>
				@if(Groups::broj_cerki($row2->grupa_pr_id) >0)
				<?php $query_category_third=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row2->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
				<ul>
					@foreach($query_category_third as $row3)
					<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}/{{ Url_mod::slug_trans($row3->grupa) }}">{{ Language::trans($row3->grupa) }}
					</a>
					@if(Groups::broj_cerki($row3->grupa_pr_id) >0)
					<?php $query_category_forth=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row3->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>

					<ul>
						@foreach($query_category_forth as $row4)
						<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}/{{ Url_mod::slug_trans($row3->grupa) }}/{{ Url_mod::slug_trans($row4->grupa) }}">{{ Language::trans($row4->grupa) }}</a>
						@endforeach
					</ul>
					@endif	
					@endforeach
				</ul>
				@endif
			</li>
			@endforeach

			@else
				<li>
					<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}">{{ Language::trans($row1->grupa) }}</a>
				</li>
			@endif 
			@endforeach				
			@endif

			@if(Options::all_category()==1)
			<li>
				<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sve-kategorije') }}">{{ Language::trans('Sve kategorije') }}</a>
			</li>
			@endif

		 </ul>	



			</div>

										
			<!-- Categories Side -->

			

		 <!-- End of side categories -->

		



 			<div class="col-md-9 col-sm-12 col-xs-12 product-page">
				@if($strana!='pretraga' and $strana!='filteri' and $strana!='proizvodjac' and $strana != 'tagovi')
					@if(isset($sub_cats) and !empty($sub_cats))
					<div class="row sub-group hidden">

						@foreach($sub_cats as $row)
						<div class="col-md-3 col-sm-3 col-xs-12 no-padding">
							<a class="flex" href="{{ Options::base_url()}}{{ $url }}/{{ Url_mod::slug_trans($row->grupa) }}">
								
								@if(isset($row->putanja_slika))
									<img src="{{ Options::domain() }}{{ $row->putanja_slika }}" alt="{{ $row->grupa }}" class=" img-responsive" />
						 		@endif
								
								<span>{{ $row->grupa }}</span>
							</a>
						</div>
						@endforeach

					</div>
					@endif
				@endif

				@yield('products_list')

			</div>

		</div>
	</div>
</main>
<!-- PRODUCTS.blade END -->


@include('shop/themes/'.Support::theme_path().'partials/footer')
 
@include('shop/themes/'.Support::theme_path().'partials/popups')


<!-- BASE REFACTORING -->
<input type="hidden" id="base_url" value="{{Options::base_url()}}" />
<input type="hidden" id="in_stock" value="{{Options::vodjenje_lagera()}}" />
<input type="hidden" id="elasticsearch" value="{{ Options::gnrl_options(3055) }}" />

<!-- js includes -->
@if(Session::has('b2c_admin'.Options::server()) OR (Options::enable_filters()==1 AND $filter_prikazi))
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
@endif
@if(Session::has('b2c_admin'.Options::server()))
<script src="{{Options::domain()}}js/tinymce_5.1.3/tinymce.min.js"></script> 
<script src="{{ Options::domain()}}js/jquery-ui.min.js"></script>
<script src="{{Options::domain()}}js/shop/front_admin/main.js"></script>
<script src="{{Options::domain()}}js/shop/front_admin/products.js"></script>
<script src="{{Options::domain()}}js/shop/front_admin/product.js"></script>
@endif

<script src="{{Options::domain()}}js/shop/translator.js"></script>
<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}main.js"></script>
<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}main_function.js"></script>
<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}cart.js"></script>

@if(Options::header_type()==1)
<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}fixed_header.js"></script>
@endif
</body>
</html>