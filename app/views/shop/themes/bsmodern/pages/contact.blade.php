@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

<!-- CONTACT.blade -->

<div class="row"> 
	<div class="col-md-3 contactPages sm-no-padd">
		<!-- Contact Pages -->
		<h4 class="text-uppercase my-1"><i class="fas fa-sitemap"></i>  Contact Menu</h4>
			<ul class="contact-pages text-uppercase">
	   @if(All::broj_cerki(Seo::get_page_id($strana)) > 0)  
                    @foreach(All::cerka_pages(Seo::get_page_id($strana)) as $row)
                    <li>    
                        <a href="{{ Options::base_url().Url_mod::page_slug($row->naziv_stranice)->slug }}">{{Url_mod::page_slug($row->naziv_stranice)->naziv}}</a> 
						<i class="fas fa-angle-double-right px-sm"></i>                  
                    </li>                     
                    @endforeach 
    	@endif  
			</div>


	<div class="col-md-9 col-sm-12 col-xs-12 sm-no-padd lineHeight">

		<br>

		<h2 class="text-uppercase"><span class="section-title"><i class="fas fa-map-marker-alt"></i>   {{ Language::trans('Kontakt informacije') }}</span></h2>

		@if(Options::company_name() != '') 
		<div class="row">
			<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Firma') }}:</div>
			<div class="col-md-7 col-sm-7 col-xs-7"> {{ Options::company_name() }}</div>
		</div> 
		@endif

		@if(Options::company_adress() != '')
		<div class="row">
			<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Adresa') }}:</div>
			<div class="col-md-7 col-sm-7 col-xs-7"> {{ Options::company_adress() }}</div>
		</div>
		@endif
		
		@if(Options::company_city() != '')
		<div class="row">
			<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Grad') }}:</div>
			<div class="col-md-7 col-sm-7 col-xs-7"> {{ Options::company_city() }}</div>
		</div>
		@endif
		
		@if(Options::company_phone() != '')
		<div class="row">
			<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Telefon') }}:</div>
			<div class="col-md-7 col-sm-7 col-xs-7"> {{ Options::company_phone() }}</div>
		</div>
		@endif
		
		@if(Options::company_fax() != '')
		<div class="row">
			<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Fax') }}:</div>
			<div class="col-md-7 col-sm-7 col-xs-7"> {{ Options::company_fax() }}</div>
		</div>
		@endif
		
		@if(Options::company_pib() != '')
		<div class="row">
			<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('PIB') }}:</div>
			<div class="col-md-7 col-sm-7 col-xs-7"> {{ Options::company_pib() }}</div>
		</div>
		@endif
		
		@if(Options::company_maticni() != '')
		<div class="row">
			<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Matični broj') }}:</div>
			<div class="col-md-7 col-sm-7 col-xs-7"> {{ Options::company_maticni() }}</div>
		</div>
		@endif
		
		@if( Options::company_email() != '')
		<div class="row">
			<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('E-mail') }}:</div>
			<div class="col-md-7 col-sm-7 col-xs-7">
				<a href="mailto:{{ Options::company_email() }}">{{ Options::company_email() }}</a>
			</div> 
		</div>
		@endif

		<div class="col-md-9 col-sm-12 col-xs-12 sm-no-padd no-padding"> 

			<br>

			<h2 class="text-uppercase"><span class="section-title"><i class="far fa-envelope"></i>  {{ Language::trans('Pošaljite poruku') }}</span></h2>

			<!-- Alert Box -->
			<div class="alert-box my-1">
				<div>
				  <i class="fas fa-info-circle px-sm"></i>
				  <p>Molimo vas da vodite računa da su vaši podaci za kontakt tačni.</p>
				</div>
			</div>

			<form method="POST" action="{{ Options::base_url() }}contact-message-send">

				<div>
					<label>{{ Language::trans('Department') }} *</label>
					<select name="contact-department">
						<option value=""></option>
						<option value="Korisnički servis" {{ ( Input::old('contact-department') == 'Korisnički servis') ? 'selected' : '' }}>{{ Language::trans('Korisnički servis') }}</option>
						<option value="Odeljenje prodaje"  {{ ( Input::old('contact-department') == 'Odeljenje prodaje') ? 'selected' : '' }}>{{ Language::trans('Odeljenje prodaje') }}</option>
					</select>
				</div> 
					<div class="error red-dot-error">{{ $errors->first('contact-department') ? $errors->first('contact-department') : "" }}</div>

				<div>
					<label id="label_name">{{ Language::trans('Vaše ime') }} *</label>
					<input class="contact-name" name="contact-name" id="JScontact_name" type="text" value="{{ Input::old('contact-name') }}">
					<div class="error red-dot-error">{{ $errors->first('contact-name') ? $errors->first('contact-name') : "" }}</div>
				</div> 

				<div>
					<label id="label_email">{{ Language::trans('Vaša e-mail adresa') }} *</label>
					<input class="contact-email" name="contact-email" id="JScontact_email" type="text" value="{{ Input::old('contact-email') }}" >
					<div class="error red-dot-error">{{ $errors->first('contact-email') ? $errors->first('contact-email') : "" }}</div>
				</div>

				<div>
					<label>{{ Language::trans('Vaš broj telefona') }} *</label>
					<input name="contact-phonenumber" type="text" value="{{ Input::old('contact-phonenumber') }}" >
					<div class="error red-dot-error">{{ $errors->first('contact-phonenumber') ? $errors->first('contact-phonenumber') : "" }}</div>
				</div>	

				<div>	
					<label id="label_message">{{ Language::trans('Vaša poruka') }} </label>
					<textarea class="contact-message" name="contact-message" rows="5" id="message">{{ Input::old('contact-message') }}</textarea>
					<div class="error red-dot-error">{{ $errors->first('contact-message') ? $errors->first('contact-message') : "" }}</div>
				</div> 

				<div class="capcha text-center"> 
					{{ Captcha::img(5, 160, 50) }}<br>
					<span>{{ Language::trans('Unesite kod sa slike') }}</span>
					<input type="text" name="captcha-string" autocomplete="off">
					<div class="error red-dot-error">{{ $errors->first('captcha') ? $errors->first('captcha') : "" }}</div>
				</div>

				<div class="text-right"> 
					<button type="submit" class="button contactBtn text-uppercase">{{ Language::trans('Pošalji') }}</button>
				</div>
			</form>
		</div>

	</div> 



</div>


@if(Options::company_map() != '' && Options::company_map() != ';')
<div class="map-frame relative">
	
	<div class="map-info">
		<h5>{{ Options::company_name() }}</h5>
		<h6>{{ Options::company_adress() }}, {{ Options::company_city() }}</h6> 
	</div>

	<iframe src="https://maps.google.com/maps?q={{ All::lat_long()[0] }},{{ All::lat_long()[1] }}&output=embed" width="100%" height="100%" frameborder="0" aria-hidden="false" tabindex="0"></iframe> 

</div> 
@endif
 

@if(Session::get('message'))
<script>
	$(document).ready(function(){     
 
        bootboxDialog({ message: "<p>{{ Session::get('message') }}</p>" }); 

	});
</script>
@endif

<!-- CONTACT.blade END -->

@endsection     