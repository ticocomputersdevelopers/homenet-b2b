<!-- HEADER.blade -->
<header>   
    <div id="JSfixed_header" {{ Options::web_options(321, 'str_data') != '' ? 'style=background-color:' . Options::web_options(321, 'str_data') : '' }}>  
        <div class="container-fluid{{(Options::web_options(321)==1) ? 'container' : '' }} no-padding"> 
            <div class="row flex rowReverseResp linear-gradient"> 


            <!-- Brayton Header Design -->
                <div class="col-md-12 col-xs-8 customHeader no-padding">
                    <div class="col-md-3 leftPiece no-padding static">
                        <div class="purple"></div>
                        <div class="gray"></div>
                    </div>

                    <div class="col-md-12 no-padding static">
                        
                        <div class="row flex-sm linear-gradient">
                            <div class="col-lg-1 col-md-2 col-sm-4 col-xs-12 px-sm">
                                <h1 class="seo">{{ Options::company_name() }}</h1>
                            
                                <a class="logo verticalTop inline-block" href="/" title="{{Options::company_name()}}" rel="nofollow">
                                    <img src="{{ Options::domain() }}{{Options::company_logo()}}" alt="{{Options::company_name()}}" class="img-responsive"/>
                                </a>
                            </div>

                            <div class="col-lg-11 col-md-10 col-sm-8 col-xs-6 no-padding static">
                                <div class="row flexEndSm">
                                    <div class="col-md-12 details">
                                        <div class="left-details social_icons hidden-sm hidden-xs">
                                            <div class="col-md-6 pl-60">
                                                <div class="social-icons hidden-sm hidden-xs">  
                                                    {{Options::social_icon()}} 
                                             </div>
                                            </div>
                                        </div>
                                        <div class="right-details">
                                            <div class="col-md-6 col-sm-12 col-xs-12 text-center"> 
                                            @if(Options::checkB2B())
                                                <a href="{{Options::domain()}}b2b/login" class="center-block" rel="nofollow">B2B</a> 
                                            @endif 
                                            </div>   
                                        </div>
                                    </div>
                                </div>



                                    <div class="row search hidden-sm hidden-xs">
                                        <div class="col-lg-6 col-md-8 no-padding static">
                                          <div id="fullNav" class="row">

                                                @if(Options::category_view()==1)
                                                    @include('shop/themes/'.Support::theme_path().'partials/categories/category')
                                                @endif               

                                                <div class="col-md-9 col-sm-12 col-xs-12 no-padding">

                                                    <ul class="main-menu">
                                                        @foreach(All::header_menu_pages() as $row)
                                                        <li>
                                                            @if(All::broj_cerki($row->web_b2c_seo_id) > 0)  
                                                            <a href="{{ Options::base_url().Url_mod::page_slug($row->naziv_stranice)->slug }}">{{Url_mod::page_slug($row->naziv_stranice)->naziv}}</a>
                                                            <ul class="drop-2">
                                                                @foreach(All::header_menu_pages($row->web_b2c_seo_id) as $row2)
                                                                <li> 
                                                                    <a href="{{ Options::base_url().Url_mod::page_slug($row2->naziv_stranice)->slug }}">{{Url_mod::page_slug($row2->naziv_stranice)->naziv}}</a>
                                                                    <ul class="drop-3">
                                                                        @foreach(All::header_menu_pages($row2->web_b2c_seo_id) as $row3)
                                                                        <li> 
                                                                            <a href="{{ Options::base_url().Url_mod::page_slug($row3->naziv_stranice)->slug }}">{{Url_mod::page_slug($row3->naziv_stranice)->naziv}}</a>
                                                                        </li>
                                                                        @endforeach
                                                                    </ul>
                                                                </li>
                                                                @endforeach
                                                            </ul>
                                                            @else   
                                                            <a href="{{ Options::base_url().Url_mod::page_slug($row->naziv_stranice)->slug }}">{{Url_mod::page_slug($row->naziv_stranice)->naziv}}</a> 
                                                            @endif                    
                                                        </li>                     
                                                        @endforeach 

                                                        @if(Options::web_options(121)==1)
                                                        <?php $konfiguratori = All::getKonfiguratos(); ?>
                                                        @if(count($konfiguratori) > 0)
                                                        @if(count($konfiguratori) > 1)
                                                        <li>
                                                            
                                                            <a href="#!" rel="nofollow">{{Language::trans('Konfiguratori')}}</a>

                                                            <ul class="drop-2">
                                                                @foreach($konfiguratori as $row)
                                                                <li>
                                                                    <a href="{{ Options::base_url() }}{{Url_mod::slug_trans('konfigurator')}}/{{ $row->konfigurator_id }}">{{ Language::trans($row->naziv) }}</a>
                                                                </li>
                                                                @endforeach
                                                            </ul>
                                                        </li>
                                                        @else
                                                        <li>
                                                            <a href="{{ Options::base_url() }}{{Url_mod::slug_trans('konfigurator')}}/{{ $konfiguratori[0]->konfigurator_id }}">
                                                                {{Language::trans('Konfigurator')}}
                                                            </a>
                                                        </li>
                                                        @endif
                                                        @endif
                                                        @endif
                                                    </ul>   
                                                </div> 
                                            </div>                                 
                                        </div>

                                        <!-- Search -->
                                         <div class="col-md-4 col-sm-8 col-xs-12 no-padding changeCol">  
                                            <div class="row header-search"> 
                                            
                                                @if(Options::gnrl_options(3055) == 0)
                                
                                                    <div class="col-md-3 col-sm-3 col-xs-3 no-padding JSselectTxt">  
                                                    
                                                        {{ Groups::firstGropusSelect('2') }} 
                                            
                                                    </div>
                                            
                                                @else  
                                            
                                                    <input type="hidden" class="JSSearchGroup2" value="">
                                            
                                                @endif 

                                                
                                                <div class="col-md-9 col-sm-9 {{ Options::gnrl_options(3055) == 0 ? 'col-xs-9' : 'col-xs-12' }} no-padding JSsearchContent2">  
                                                    <div class="relative"> 
                                                        <form autocomplete="off">
                                                            <input type="text" id="JSsearch2" placeholder="{{ Language::trans('Pretraga') }}" />
                                                        </form>      
                                                        <button onclick="search2()" class="JSsearch-button2"> <i class="fas fa-search"></i> </button>
                                                    </div>
                                                </div> 
                                            </div> 
                                        </div>
                                    <!-- End of search -->


                                    </div>
            
                                </div>                  
                            </div>     
                         </div>
                     </div>



                <!-- End of brayton header -->


                <!-- <div class="col-md-3 col-sm-4 col-xs-12">
                    
                    <h1 class="seo">{{ Options::company_name() }}</h1>
                    
                    <a class="logo v-align inline-block" href="/" title="{{Options::company_name()}}" rel="nofollow">
                        <img src="{{ Options::domain() }}{{Options::company_logo()}}" alt="{{Options::company_name()}}" class="img-responsive"/>
                    </a>
                </div> -->

                <!-- <div class="col-md-5 col-sm-8 col-xs-12">  
                    <div class="row header-search"> 
                    
                        @if(Options::gnrl_options(3055) == 0)
        
                            <div class="col-md-3 col-sm-3 col-xs-3 no-padding JSselectTxt">  
                               
                                {{ Groups::firstGropusSelect('2') }} 
                    
                            </div>
                    
                        @else  
                    
                            <input type="hidden" class="JSSearchGroup2" value="">
                    
                        @endif 

                         
                        <div class="col-md-9 col-sm-9 {{ Options::gnrl_options(3055) == 0 ? 'col-xs-9' : 'col-xs-12' }} no-padding JSsearchContent2">  
                            <div class="relative"> 
                                <form autocomplete="off">
                                    <input type="text" id="JSsearch2" placeholder="{{ Language::trans('Pretraga') }}" />
                                </form>      
                                <button onclick="search2()" class="JSsearch-button2"> <i class="fas fa-search"></i> </button>
                            </div>
                        </div> 
                    </div> 
                </div> -->

                <!-- <div class="col-md-3 col-sm-6 col-xs-7 text-center">
                    @if(Session::has('b2c_kupac'))

                        <span class="JSbroj_wish far fa-heart">{{ Cart::broj_wish() }}</span>  

                        <a href="{{Options::base_url()}}{{Url_mod::slug_trans('korisnik')}}/{{Url_mod::slug_trans(WebKupac::get_user_name())}}" rel="nofollow">{{ WebKupac::get_user_name() }}</a>

                        <a href="{{Options::base_url()}}{{Url_mod::slug_trans('korisnik')}}/{{Url_mod::slug_trans(WebKupac::get_company_name())}}" rel="nofollow">{{ WebKupac::get_company_name() }}</a>

                        <a id="logout-button" class="inline-block" href="{{Options::base_url()}}logout" rel="nofollow">{{ Language::trans('Odjavi se') }}</a>

                    @else 
                        <div class="dropdown inline-block">
                            <button class="dropdown-toggle login-btn" type="button" data-toggle="dropdown">
                                <span class="fas fa-sign-in-alt"></span> &nbsp;{{ Language::trans('Moj nalog') }} <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu login-dropdown"> -->
                                <!-- ====== LOGIN MODAL TRIGGER ========== -->
                                <!-- <li>
                                    <a href="#" data-toggle="modal" data-target="#loginModal" rel="nofollow">
                                    <i class="fas fa-user"></i> {{ Language::trans('Prijavi se') }}</a>
                                </li>
                                <li>
                                    <a href="{{Options::base_url()}}{{ Url_mod::slug_trans('registracija') }}" rel="nofollow"> 
                                    <i class="fas fa-user-plus"></i> {{ Language::trans('Registracija') }}</a>
                                </li>
                            </ul>
                        </div> 
                    @endif
                </div> -->
 
                @include('shop/themes/'.Support::theme_path().'partials/cart_top')

                <!-- RESPONSIVE BUTTON -->
                <div class="resp-nav-btn hidden-md hidden-lg"><span class="fas fa-bars"></span></div>

            </div> 
        </div>  
    </div> 
</header>

<div class="menu-background hidden-lg hidden-md">   
    <div class="container"> 
        <div id="responsive-nav" class="row">

            @if(Options::category_view()==1)
                @include('shop/themes/'.Support::theme_path().'partials/categories/category')
            @endif               

            <div class="col-md-9 col-sm-12 col-xs-12">

                <ul class="main-menu text-white">
                    @foreach(All::header_menu_pages() as $row)
                    <li>
                        @if(All::broj_cerki($row->web_b2c_seo_id) > 0)  
                        <a href="{{ Options::base_url().Url_mod::page_slug($row->naziv_stranice)->slug }}">{{Url_mod::page_slug($row->naziv_stranice)->naziv}}</a>
                        <ul class="drop-2">
                            @foreach(All::header_menu_pages($row->web_b2c_seo_id) as $row2)
                            <li> 
                                <a href="{{ Options::base_url().Url_mod::page_slug($row2->naziv_stranice)->slug }}">{{Url_mod::page_slug($row2->naziv_stranice)->naziv}}</a>
                                <ul class="drop-3">
                                    @foreach(All::header_menu_pages($row2->web_b2c_seo_id) as $row3)
                                    <li> 
                                        <a href="{{ Options::base_url().Url_mod::page_slug($row3->naziv_stranice)->slug }}">{{Url_mod::page_slug($row3->naziv_stranice)->naziv}}</a>
                                    </li>
                                    @endforeach
                                </ul>
                            </li>
                            @endforeach
                        </ul>
                        @else   
                        <a href="{{ Options::base_url().Url_mod::page_slug($row->naziv_stranice)->slug }}">{{Url_mod::page_slug($row->naziv_stranice)->naziv}}</a> 
                        @endif                    
                    </li>                     
                    @endforeach 

                    @if(Options::web_options(121)==1)
                    <?php $konfiguratori = All::getKonfiguratos(); ?>
                    @if(count($konfiguratori) > 0)
                    @if(count($konfiguratori) > 1)
                    <li>
                        
                        <a href="#!" rel="nofollow">{{Language::trans('Konfiguratori')}}</a>

                        <ul class="drop-2">
                            @foreach($konfiguratori as $row)
                            <li>
                                <a href="{{ Options::base_url() }}{{Url_mod::slug_trans('konfigurator')}}/{{ $row->konfigurator_id }}">{{ Language::trans($row->naziv) }}</a>
                            </li>
                            @endforeach
                        </ul>
                    </li>
                    @else
                    <li>
                        <a href="{{ Options::base_url() }}{{Url_mod::slug_trans('konfigurator')}}/{{ $konfiguratori[0]->konfigurator_id }}">
                            {{Language::trans('Konfigurator')}}
                        </a>
                    </li>
                    @endif
                    @endif
                    @endif
                </ul>   
            </div> 
        </div>
    </div>    
</div> 


<div class="modal fade" id="loginModal" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content" >
            <div class="modal-header text-center">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <p class="modal-title">{{ Language::trans('Dobrodošli') }}</p>
                <p class="no-margin">{{ Language::trans('Za pristup Vašem nalogu unesite Vaš e-mail i lozinku') }}.</p>
            </div>

            <div class="modal-body"> 
                <label for="JSemail_login">{{ Language::trans('E-mail') }}</label>
                <input id="JSemail_login" type="text" value="" autocomplete="off">
        
                <label for="JSpassword_login">{{ Language::trans('Lozinka') }}</label>
                <input autocomplete="off" id="JSpassword_login" type="password" value=""> 
            </div>

            <div class="modal-footer text-right">
                <a class="inline-block button" href="{{Options::base_url()}}{{ Url_mod::slug_trans('registracija') }}">{{Language::trans('Registruj se')}}</a>
                
                <button type="button" onclick="user_login()" class="button">{{ Language::trans('Prijavi se') }}</button>
                
                <button type="button" onclick="user_forgot_password()" class="forgot-psw pull-left">{{ Language::trans('Zaboravljena lozinka') }}</button>

                <div class="field-group error-login JShidden-msg" id="JSForgotSuccess"><br>
                    {{ Language::trans('Novu lozinku za logovanje dobili ste na navedenoj e-mail adresi') }}.
                </div> 
            </div>
        </div>   
    </div>
</div>
<!-- HEADER.blade END -->