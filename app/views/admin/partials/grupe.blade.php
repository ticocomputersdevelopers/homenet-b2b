<?php
$query_gr=AdminSupport::query_gr_level(0);
?>
<!-- ====================== -->
<div>
	@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
	<a class="btn btn-create" href="{{AdminOptions::base_url()}}admin/grupe/0">{{ AdminLanguage::transAdmin('Napravi novu grupu') }}</a> <a href="#" class="video-manual" data-reveal-id="encoders-manual">{{ AdminLanguage::transAdmin('Uputstvo') }}<i class="fa fa-film"></i></a>
	@endif
	<div id="encoders-manual" class="reveal-modal medium" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
		<div class="video-manual-container"> 
			<p><span class="video-manual-title">{{ AdminLanguage::transAdmin('Grupe i Šifarnici') }}</span></p>
			<iframe src="https://player.vimeo.com/video/271251005" width="840" height="426" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
		</div>
		<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	</div>
</div>
<!-- ====================== -->
<!-- LEVEL 1 --> 
<div id="jstree" class="categories">
	<ul>
		<li data-id="0" data-parentid="-1" class="jstree-open">
			<a>{{ AdminLanguage::transAdmin('GRUPE') }}</a>
			<ul>
				@foreach($query_gr as $row)
				<li data-id="{{ $row->grupa_pr_id }}" data-parentid="{{ $row->parrent_grupa_pr_id }}" class="{{ AdminSupport::categoryOpen($grupa_pr_id,$grupa_parents,$row->grupa_pr_id) }}">

					<a href="{{AdminOptions::base_url()}}admin/grupe/{{ $row->grupa_pr_id }}" data-id="{{ $row->grupa_pr_id }}">{{ $row->grupa }}</a>
					<?php $query_gr1=AdminSupport::query_gr_level($row->grupa_pr_id); ?>
					<!-- LEVEL 2 -->
					<ul>
						@foreach($query_gr1 as $row1)
						<li data-id="{{ $row1->grupa_pr_id }}" data-parentid="{{ $row1->parrent_grupa_pr_id }}" class="{{ AdminSupport::categoryOpen($grupa_pr_id,$grupa_parents,$row1->grupa_pr_id) }}">
							<a href="{{AdminOptions::base_url()}}admin/grupe/{{ $row1->grupa_pr_id }}" data-id="{{ $row1->grupa_pr_id }}">{{ $row1->grupa }}</a>
							<?php $query_gr2=AdminSupport::query_gr_level($row1->grupa_pr_id); ?>
							<!-- LEVEL 3 -->
							<ul>
								@foreach($query_gr2 as $row2)
								<li data-id="{{ $row2->grupa_pr_id }}" data-parentid="{{ $row2->parrent_grupa_pr_id }}" class="{{ AdminSupport::categoryOpen($grupa_pr_id,$grupa_parents,$row2->grupa_pr_id) }}">
									<a href="{{AdminOptions::base_url()}}admin/grupe/{{ $row2->grupa_pr_id }}" data-id="{{ $row2->grupa_pr_id }}">{{ $row2->grupa }}</a>
									<?php $query_gr3=AdminSupport::query_gr_level($row2->grupa_pr_id); ?>
									<!-- LEVEL 4 -->
									<ul>
										@foreach($query_gr3 as $row3)
										<li data-id="{{ $row3->grupa_pr_id }}" data-parentid="{{ $row3->parrent_grupa_pr_id }}" class="{{ AdminSupport::categoryOpen($grupa_pr_id,$grupa_parents,$row3->grupa_pr_id) }}">
											<a href="{{AdminOptions::base_url()}}admin/grupe/{{ $row3->grupa_pr_id }}" data-id="{{ $row3->grupa_pr_id }}">{{ $row3->grupa }}</a>
										</li>
										@endforeach
									</ul>
								</li>
								@endforeach
							</ul>
						</li>
						@endforeach
					</ul>
				</li>
				@endforeach
			</ul>
		</li>
	</ul>
</div>