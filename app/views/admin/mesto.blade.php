<section class="" id="main-content">

	@include('admin/partials/tabs')

	<div class="row">
		<section class="small-12 medium-12 large-3 columns">
			<div class="flat-box">
				<h3 class="title-med">{{ AdminLanguage::transAdmin('Izaberi mesto') }}</h3>
				<select class="JSeditSupport">
					<option value=""></option>
					<option value="{{ AdminOptions::base_url() }}admin/mesto">{{ AdminLanguage::transAdmin('Dodaj novo') }}</option>
					@foreach($mesta as $row)
						<option value="{{ AdminOptions::base_url() }}admin/mesto/{{ $row->mesto_id }}"{{ $row->mesto_id == $mesto_id ? 'selected' : '' }}>{{ $row->mesto }}</option>
					@endforeach
				</select>
			</div>
		</section>

		<section class="small-12 medium-12 large-5 columns">
			<div class="flat-box">
				<h1 class="title-med">{{ $title }}</h1>

				<form method="POST" action="{{ AdminOptions::base_url() }}admin/mesto-edit" enctype="multipart/form-data">
					<div class="row">
						<input type="hidden" name="mesto_id" value="{{ $mesto_id }}">
						<div class="columns medium-6 field-group{{ $errors->first('mesto') ? ' error' : '' }}">
							<label for="">{{ AdminLanguage::transAdmin('Mesto') }}</label>
							<input type="text" name="mesto" value="{{ htmlentities(Input::old('mesto') ? Input::old('mesto') : $mesto) }}" autofocus="autofocus">
						</div>
						<div class="columns medium-6 field-group{{ $errors->first('ptt') ? ' error' : '' }}">
							<label for="">{{ AdminLanguage::transAdmin('Poštanski broj') }}</label>
							<input type="text" name="ptt" value="{{ Input::old('ptt') ? Input::old('ptt') : $ptt }}" >
						</div>
						<div class="btn-container center no-margin-bottom">
							<button type="submit" class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
							@if($mesto_id != null)
							<button class="btn btn-danger JSbtn-delete" data-link="{{ AdminOptions::base_url() }}admin/mesto-delete/{{ $mesto_id }}">{{ AdminLanguage::transAdmin('Obriši') }}</button>
							@endif
						</div>
					</div>
				</form>
				<div class="btn-container center no-margin-bottom">
					@if(Session::get('message'))
						{{ AdminLanguage::transAdmin('Nemoguće je obrisati ovo mesto jer je vezano za kupca ili partnera') }}!
					@endif
				</div>
			 
			</div>
		</section>

	</div>
  <!-- </form> -->
</section>