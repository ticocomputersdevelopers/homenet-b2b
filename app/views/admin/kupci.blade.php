<div id="main-content" class="kupci-page">
	@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
	@endif
	
	@if(in_array(AdminOptions::web_options(130),array(0)))
 
		@include('admin/partials/sifarnik-tabs')
  
	@endif
 
	@if(Admin_model::check_admin(array('KUPCI_I_PARTNERI_AZURIRANJE')))
 
		@if(AdminOptions::web_options(130) != 0) 
		<h3 class="title-med">Kupci</h3>

		@endif

		<a class="btn btn-create btn-small" href="{{AdminOptions::base_url()}}admin/kupci_partneri/kupci/0">{{ AdminLanguage::transAdmin('Novi kupac') }}</a> 
 
	@endif 

	<div class="flat-box padding-v-8">
		<div class="row collapse">
			<div class="columns medium-9"> 
				<div class="row collapse">
					<div class="columns medium-7 no-padd">
						<a href="{{AdminOptions::base_url()}}admin/kupci_partneri/kupci" class="JS-svi btn btn-primary btn-small {{ ($vrsta_kupca == 'n' AND $status_registracije == 'n') ? 'filter_active' : '' }}">{{ AdminLanguage::transAdmin('Svi kupci') }}</a>
						<a href="{{AdminOptions::base_url()}}admin/kupci_partneri/kupci/0/{{$status_registracije}}/{{$search}}" class="JS-svi btn btn-primary btn-small {{ $vrsta_kupca == '0' ? 'filter_active' : '' }}">{{ AdminLanguage::transAdmin('Fizička lica') }}</a>
						<a href="{{AdminOptions::base_url()}}admin/kupci_partneri/kupci/1/{{$status_registracije}}/{{$search}}" class="JS-svi btn btn-primary btn-small {{ $vrsta_kupca == '1' ? 'filter_active' : '' }}">{{ AdminLanguage::transAdmin('Pravna lica') }}</a>
						<a href="{{AdminOptions::base_url()}}admin/kupci_partneri/kupci/{{$vrsta_kupca}}/1/{{$search}}" class="JS-svi btn btn-primary btn-small {{ $status_registracije == '1' ? 'filter_active' : '' }}">{{ AdminLanguage::transAdmin('Registrovani') }}</a>
						<a href="{{AdminOptions::base_url()}}admin/kupci_partneri/kupci/{{$vrsta_kupca}}/0/{{$search}}" class="JS-svi btn btn-primary btn-small {{ $status_registracije == '0' ? 'filter_active' : '' }}">{{ AdminLanguage::transAdmin('Neregistrovani') }}</a>
					</div>
					<div class="columns medium-5">
						<form method="POST" action="{{AdminOptions::base_url()}}admin/kupci_partneri/kupci/{{$vrsta_kupca}}/{{$status_registracije}}" class="m-input-and-button">
							<input type="text" name="search" value="{{urldecode($search)}}" placeholder="{{ AdminLanguage::transAdmin('Pretraga') }}..." class="m-input-and-button__input">
							<input class="btn btn-primary btn-small kupci-btn" type="submit" value="Pretraga" class="m-input-and-button__btn">
							<a class="btn btn-danger btn-small" href="{{AdminOptions::base_url()}}admin/kupci_partneri/kupci">{{ AdminLanguage::transAdmin('Poništi') }}</a>
						</form>
					</div>
				</div> 
			</div>

			<div class="columns medium-3">
				<div class="text-right">
					<a class="JSExportXML btn btn-primary btn-small" href="#">{{ AdminLanguage::transAdmin('Export') }}</a>
					<a class="btn btn-primary btn-small" href="{{AdminOptions::base_url()}}admin/kupci_partneri/export_newslette">{{ AdminLanguage::transAdmin('Newsletter') }}</a>
				</div>
			</div>

			<div id="JSchooseXml" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
				<a class="close-reveal-modal" aria-label="Close">&#215;</a>
				<h4 class="modal-title-h4">{{ AdminLanguage::transAdmin('Export') }}</h4>
				<div class="column medium-12 large-12">
					<h3 class="text-center">{{ AdminLanguage::transAdmin('Izaberite željene kolone') }}:</h3>
					<div class="row">
						@if($vrsta_kupca == "1")
						<input type="checkbox" data-name="naziv" class="JSKolonaExport" data-status="naziv">Naziv') }}
						<input type="checkbox" data-name="pib" class="JSKolonaExport" data-status="pib">{{ AdminLanguage::transAdmin('Pib') }}
						@elseif($vrsta_kupca == "0")
						<input type="checkbox" data-name="ime" class="JSKolonaExport" data-status="ime">{{ AdminLanguage::transAdmin('Ime') }}
						<input type="checkbox" data-name="prezime" class="JSKolonaExport" data-status="prezime">Prezime') }}
						@else
						<input type="checkbox" data-name="naziv" class="JSKolonaExport" data-status="naziv">{{ AdminLanguage::transAdmin('Naziv') }}
						<input type="checkbox" data-name="pib" class="JSKolonaExport" data-status="pib">{{ AdminLanguage::transAdmin('Pib') }}
						<input type="checkbox" data-name="ime" class="JSKolonaExport" data-status="ime">{{ AdminLanguage::transAdmin('Ime') }}
						<input type="checkbox" data-name="prezime" class="JSKolonaExport" data-status="prezime">{{ AdminLanguage::transAdmin('Prezime') }}						
						@endif
						<input type="checkbox" data-name="adresa" class="JSKolonaExport" data-status="adresa">{{ AdminLanguage::transAdmin('Adresa') }}
						<input type="checkbox" data-name="mesto" class="JSKolonaExport" data-status="mesto">{{ AdminLanguage::transAdmin('Mesto') }}
						<input type="checkbox" data-name="email" class="JSKolonaExport" data-status="email">{{ AdminLanguage::transAdmin('Email') }}
						<input type="checkbox" data-name="telefon" class="JSKolonaExport" data-status="telefon">{{ AdminLanguage::transAdmin('Telefon') }}
					</div>
					<button data-link="{{AdminOptions::base_url()}}admin/kupci_partneri/export_kupac/xml/{{$vrsta_kupca}}/{{$status_registracije}}/null/{{urlencode($search)}}" class="btn btn-primary m-input-and-button__btn JSExportKupci" >{{ AdminLanguage::transAdmin('Export XML') }}</button>

					<button data-link="{{AdminOptions::base_url()}}admin/kupci_partneri/export_kupac/xls/{{$vrsta_kupca}}/{{$status_registracije}}/null/{{urlencode($search)}}" class="btn btn-primary m-input-and-button__btn JSExportKupci" >{{ AdminLanguage::transAdmin('Export XLS') }}</button>
				</div> 
			</div>
		</div>
	</div>
 
	<div class="flat-box">
		<div class="table-scroll"> 
			<table>
				<tr>
					<th>{{ AdminLanguage::transAdmin('Naziv') }}</th>
					<th>{{ AdminLanguage::transAdmin('Adresa') }}</th>
					<th>{{ AdminLanguage::transAdmin('Mesto') }}</th>
					<th>{{ AdminLanguage::transAdmin('Email') }}</th>
					<th>{{ AdminLanguage::transAdmin('Telefon') }}</th>
					@if(Admin_model::check_admin(array('KUPCI_I_PARTNERI_AZURIRANJE')))
					<td></td>
					@endif
				</tr>
				@foreach($web_kupac as $row)
				<tr>
					@if($row->flag_vrsta_kupca == 0)
					<td>{{ $row->ime }} {{ $row->prezime }}</td>
					@else
					<td>{{ $row->naziv }}</td>
					@endif
					<td>{{ $row->adresa }}</td>
					<td>{{ $row->mesto }}</td>
					<td>{{ $row->email }}</td>
					<td>{{ $row->telefon }}</td>
					@if(Admin_model::check_admin(array('KUPCI_I_PARTNERI_AZURIRANJE')))
					<td class="table-col-icons">
						<a class="edit icon" href="{{AdminOptions::base_url()}}admin/kupci_partneri/kupci/{{ $row->web_kupac_id }}"><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;{{ AdminLanguage::transAdmin('Izmeni') }}</a>
						<a class="remove icon JSbtn-delete" data-link="{{AdminOptions::base_url()}}admin/kupci_partneri/kupci/{{ $row->web_kupac_id }}/0/delete"><i class="fa fa-trash" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;{{ AdminLanguage::transAdmin('Obriši') }}</a>
						@if($row->flag_vrsta_kupca == 1)
						<a class="save-as-partner icon" href="{{AdminOptions::base_url()}}admin/kupci_partneri/kupci/{{ $row->web_kupac_id }}/snimi_kao_partner"><i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;{{ AdminLanguage::transAdmin('Snimi kao partnera') }}</a>
						@endif
					</td>
					@endif
				</tr>
				@endforeach
			</table>
		</div>
		{{ $web_kupac->links() }}
	</div>
 
	<input type="hidden" id="JSCheckConfirm" data-id="{{ Session::has('confirm_kupac_id')?Session::get('confirm_kupac_id'):0 }}"> 
</div>