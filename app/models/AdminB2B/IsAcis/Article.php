<?php
namespace IsAcis;
use DB;
use Options;
use All;

class Article {

	public static function table_body($articles,$mappedGroups){
		$kurs = DB::table('kursna_lista')->where('kursna_lista_id',DB::table('kursna_lista')->max('kursna_lista_id'))->pluck('ziralni');
		$defaultGroupId = -1;

		$result_arr = array();

		$roba_id =  DB::table('roba')->max('roba_id')+1;
		$sifra_k = DB::table('roba')->max('sifra_k')+1;

		foreach($articles as $articleObj) {
			foreach($articleObj as $article) {

			if(isset($article->Id_Artikla)){
				$id_is = $article->Id_Artikla;
				$roba_id++;
				$sifra_k++;

				$sifra_is = isset($article->Sifra_Artikla) ? pg_escape_string($article->Sifra_Artikla) : '';
				$naziv = isset($article->Naziv_Artikla) ? Support::convert(substr($article->Naziv_Artikla,0,300)) : '';
				$web_opis = '';
				$jedinica_mere_id = isset($article->Jedinica_Mere) && $article->Jedinica_Mere != '' ? Support::getJedinicaMereId(strtolower($article->Jedinica_Mere)) : 1;
				$proizvodjac_id = isset($article->Proizvodjac_Naziv) && trim($article->Proizvodjac_Naziv) != '' ? Support::getProizvodjacId(Support::convert($article->Proizvodjac_Naziv)) : -1;
				$sifra_d=isset($article->Sifra_Kod_Proizvodjaca) ? pg_escape_string($article->Sifra_Kod_Proizvodjaca) : '';
				$vrednost_tarifne_grupe = isset($article->Tarifa_Stopa) && is_numeric($article->Tarifa_Stopa) ? $article->Tarifa_Stopa : 20;
				$tarifna_grupa_id = Support::getTarifnaGrupaId(strval($vrednost_tarifne_grupe),$vrednost_tarifne_grupe);
				$grupa_pr_id = isset($mappedGroups[$article->Artikal_Grupa_ID]) ? $mappedGroups[$article->Artikal_Grupa_ID] : -1;

				$barkod = "NULL";
				// $grupa_pr_id = Support::getGrupaId('NOVI ARTIKLI');
				// $kursValuta = trim($article->acCurrency) != 'EUR' ? 1 : $kurs;
				//$kursValuta = floatval($article->anFxRate);

				$racunska_cena_nc = 0.00;
				$mpcena = 0.00;
				$web_cena = 0.00;
				$racunska_cena_a = 0.00;
				$racunska_cena_end = 0.00;

				// $roba_cene = Support::roba_cene($id_is);
				// $stara_cena = !is_null($roba_cene) ? ($roba_cene->racunska_cena_end != $racunska_cena_end ? $roba_cene->racunska_cena_end : $roba_cene->stara_cena) : $racunska_cena_end;
				$stara_cena = 0.00;

				$marza = 0;
				$flag_aktivan = '1';
				// $flag_cenovnik = $racunska_cena_nc > 0 ? '1' : '0';
				$flag_cenovnik = '1';
				$sku = $roba_id;
				// $garancija = isset($article->Warranty) && is_numeric($article->Warranty) ? $article->Warranty : "0";
				$garancija = 0;
		
				$result_arr[] = "(".strval($roba_id).",'".$sifra_d."','".$naziv."',NULL,NULL,NULL,".$grupa_pr_id.",".strval($tarifna_grupa_id).",".strval($jedinica_mere_id).",".strval($proizvodjac_id).",-1,".strval($sifra_k).",NULL,NULL,'".substr($naziv,0,20)."',0,-1,0,0,0,0,9,0,0,0,".strval($garancija).",1,".$flag_cenovnik.",0,NULL,".$flag_aktivan.",".strval($racunska_cena_a).",".strval($racunska_cena_end).",".strval($racunska_cena_nc).",0,NULL,".strval($mpcena).",false,".strval($marza).",(NULL)::integer,'".$naziv."',".$flag_cenovnik.",NULL,('".date('Y-m-d H:i:s')."')::timestamp,(NULL)::integer,(NULL)::integer,0,0,0,-1,-1,".strval($web_cena).",1,0,'".$web_opis."',NULL,NULL,0,0,0,NULL,NULL,NULL,NULL,1,0,".$barkod.",".strval($stara_cena).",0,1,1,-1,NULL,NULL,NULL,NULL,0,0.00,0.00,0.00,0,".$sku.",(NULL)::date,(NULL)::date,(NULL)::integer,NULL,'".strval($sifra_is)."','".strval($id_is)."',0,0,0,(NULL)::date,(NULL)::date,0.00,0.00,1,NULL)";
			
			}
		}

		return (object) array("body"=>implode(",",$result_arr));
		}
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}

		$all_columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$columns = $all_columns;
		$columns_without_id = $all_columns;
		unset($columns_without_id[0]);
		
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";

		DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		
		// update
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="roba_id" && $col!="id_is"){
		    	$updated_columns[] = "".$col." = roba_temp.".$col."";
			}
		}

		DB::statement("UPDATE roba t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.id_is=roba_temp.id_is::varchar AND t.id_is IS NOT NULL AND t.id_is <> '' AND t.flag_zakljucan='false'");	
		//insert
		DB::statement("INSERT INTO roba(".implode(',',$columns_without_id).") SELECT ".implode(',',$columns_without_id)." FROM ".$table_temp." WHERE roba_temp.id_is NOT IN (SELECT id_is FROM roba WHERE id_is IS NOT NULL AND id_is <> '')");


		DB::statement("SET CLIENT_ENCODING TO 'UTF8'");

		DB::statement("SELECT setval('roba_roba_id_seq', (SELECT MAX(roba_id) FROM roba) + 1, FALSE)");
	}
	public static function query_update_unexists($table_temp_body) {

		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";

		DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		//DB::statement("UPDATE roba t SET flag_aktivan = 0 WHERE t.id_is IS NOT NULL AND NOT EXISTS(SELECT * FROM ".$table_temp." WHERE t.id_is=roba_temp.id_is)");
		DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
	}
	public static function query_delete_unexists($table_temp_body) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";

		DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		DB::statement("DELETE FROM roba WHERE id_is NOT IN (SELECT id_is FROM ".$table_temp.")");
		DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
	}

}