<?php

class Wings {

	public static function autorize($username,$password){

	    $curl = curl_init();
	    curl_setopt_array($curl, array(
	      CURLOPT_URL => "https://portal.wings.rs/api/v1/".B2BOptions::info_sys('wings')->portal."/system.user.log?output=jsonapi",
	      CURLOPT_RETURNTRANSFER => true,
	      CURLOPT_ENCODING => "",
	      CURLOPT_MAXREDIRS => 10,
	      CURLOPT_TIMEOUT => 30,
	      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	      CURLOPT_CUSTOMREQUEST => "POST",
	      CURLOPT_POSTFIELDS => '{"aUn": "'.$username.'","aUp": "'.$password.'"}',
	      CURLOPT_HTTPHEADER => array(
	        "Content-Type: application/json"
	      ),
	    ));
	    $response = curl_exec($curl);
	    $err = curl_error($curl);
	    curl_close($curl);

	    if ($err) {
	      $error = "cURL Error #:" . $err;
	      return false;
	    } else {
	      $result = json_decode($response);
	      return $result;
	    }		
	}

	public static function logout($access_token){

		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://portal.wings.rs/api/v1/".B2BOptions::info_sys('wings')->portal."/system.user.log?logout=1",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "Cookie: PHPSESSID=".$access_token.""
		  ),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);	
	}

	public static function kupac_info($access_token){
		$url = "https://portal.wings.rs/api/v1/".B2BOptions::info_sys('wings')->portal."/partner.kupac.info?output=jsonapi";

		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "Cookie: PHPSESSID=".$access_token.""
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

	    if ($err) {
	      $error = "cURL Error #:" . $err;
	      return false;
	    } else {
	      $result = json_decode($response);
	      return $result;
	    }		
	}

	public static function order($access_token,$payload_body){
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://portal.wings.rs/api/v1/".B2BOptions::info_sys('wings')->portal."/dokument.narudzbenica.partner?output=jsonapi",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => $payload_body,
		  CURLOPT_HTTPHEADER => array(
		    "Content-Type: application/json",
		    "Cookie: PHPSESSID=".$access_token.""
		  ),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);

	    if ($err) {
	      $error = "cURL Error #:" . $err;
	      return false;
	    } else {
	      $result = json_decode($response);
	      return $result;
	    }		
	}

	public static function order_rabate($access_token,$payload_body){
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://portal.wings.rs/api/v1/".B2BOptions::info_sys('wings')->portal."/local.proxy.narudzbenica?output=jsonapi",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => $payload_body,
		  CURLOPT_HTTPHEADER => array(
		    "Content-Type: application/json",
		    "Cookie: PHPSESSID=".$access_token.""
		  ),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);

	    if ($err) {
	      $error = "cURL Error #:" . $err;
	      return false;
	    } else {
	      $result = json_decode($response);
	      return $result;
	    }		
	}

	public static function order_info($access_token,$order_id){
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://portal.wings.rs/api/v1/".B2BOptions::info_sys('wings')->portal."/izvestaj.narudzbenica.dokument?profaktura=".strval($order_id)."&output=jsonapi",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "Cookie: PHPSESSID=".$access_token.""
		  ),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);

	    if ($err) {
	      $error = "cURL Error #:" . $err;
	      return false;
	    } else {
	      $result = json_decode($response);
	      return $result;
	    }		
	}

	public static function all_orders_info($access_token){
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://portal.wings.rs/api/v1/".B2BOptions::info_sys('wings')->portal."/local.narudzbenica.kupac?output=jsonapi&status=-",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "Cookie: PHPSESSID=".$access_token.""
		  ),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);

	    if ($err) {
	      $error = "cURL Error #:" . $err;
	      return false;
	    } else {
	      $result = json_decode($response);
	      return $result;
	    }		
	}

	public static function kartica_kupca($access_token,$partner_id_is,$status="K"){
		$url = "https://portal.wings.rs/api/v1/".B2BOptions::info_sys('wings')->portal."/partner.kartica.partner?partner=".strval($partner_id_is)."&status=".$status."&output=jsonapi";
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "Cookie: PHPSESSID=".$access_token.""
		  ),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);

	    if ($err) {
	      $error = "cURL Error #:" . $err;
	      return false;
	    } else {
	      $result = json_decode($response);
	      return $result;
	    }		
	}


    public static function wingsOrder($cartItems){
    	$partner = DB::table('partner')->where('partner_id',Session::get('b2b_user_'.B2bOptions::server()))->first();
        $wingsItems = array(
		    'partner'=> intval($partner->id_is),
		    'status'=> 'K',
		    'valuta'=> date('m.d.y'),
		    'rok'=> 5,
		    'isporuka'=> '',
		    'objekat'=> '',
		    'magacin'=> '',
        	'artikal'=>array()
        	);
        $success = false;
        $order_id = 0;
        foreach($cartItems as $row){
            $roba = DB::table('roba')->where('roba_id',$row->roba_id)->first();
            if(isset($roba->id_is) && $roba->id_is != null){
                $wingsItems['artikal'][]=[
                  'id'=> intval($roba->id_is),
                  'kolicina'=>intval($row->kolicina),
                  'rabat'=>B2bArticle::b2bRabatCene($row->roba_id)->ukupan_rabat
                ];
            }
        }

// $payload = json_encode($wingsItems);
// echo $payload; die;        
        if(count($wingsItems['artikal']) > 0){
	        $payload = json_encode($wingsItems);
	        $wings_data = B2bOptions::info_sys('wings');
	        $username = $wings_data->username;
	        $password = $wings_data->password;
			$result = self::autorize($username,$password);
            if(!($result == false || isset($result->errors))){
                $token = $result->data[0]->attributes->token;
		        $response = self::order_rabate($token,$payload);
		        if(!isset($response->errors) && isset($response->data) && isset($response->data->id)){
		        	$order_id = intval($response->data->id);
		        	$success = true;
		        }
		        self::logout($token);
            }
	    }
	    return (object) array('success'=>$success, 'order_id'=>$order_id);
    }


	public static function kartica_kupca_body($items,$partner_id){
		$result_arr = array();

		$web_b2b_kartica_id = DB::select("SELECT nextval('web_b2b_kartica_web_b2b_kartica_id_seq')")[0]->nextval;

		foreach($items as $item) {
			$id_is = $item->id;
			$item = $item->attributes;

			$web_b2b_kartica_id++;
			$datum_dokumenta = $item->datum;
			$vrsta_dokumenta = $item->dokument;
			$duguje = $item->duguje;
			$potrazuje = $item->potrazuje;
			$saldo = $item->preostalo;

			$result_arr[] = "(".$web_b2b_kartica_id.",".strval($partner_id).",(NULL)::integer,'".$vrsta_dokumenta."',('".$datum_dokumenta."')::date,('".$datum_dokumenta."')::date,'".$vrsta_dokumenta."',".strval($duguje).",".strval($potrazuje).",".strval($saldo).",(NULL)::integer,'".strval($id_is)."')";
		}

		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function kartica_kupca_insert_update($table_temp_body,$partner_id,$upd_cols=array()) {

		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='web_b2b_kartica'"));
		$table_temp = "(VALUES ".$table_temp_body.") web_b2b_kartica_temp(".implode(',',$columns).")";

		DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		// update
		$updated_columns=array();
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="web_b2b_kartica_id"){
		    	$updated_columns[] = "".$col." = web_b2b_kartica_temp.".$col."";
			}
		}
		DB::statement("UPDATE web_b2b_kartica t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.id_is=web_b2b_kartica_temp.id_is");

		//insert
		DB::statement("INSERT INTO web_b2b_kartica (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM web_b2b_kartica t WHERE t.id_is=web_b2b_kartica_temp.id_is))");
		DB::statement("SET CLIENT_ENCODING TO 'UTF8'");

		DB::statement("SELECT setval('web_b2b_kartica_web_b2b_kartica_id_seq', (SELECT MAX(web_b2b_kartica_id) FROM web_b2b_kartica), FALSE)");
	}

	public static function updateKartica($partner_id=null){
        if(is_null($partner_id)){
            $partner_id = Session::get('b2b_user_'.B2bOptions::server());
        }
		$partner = DB::table('partner')->where('partner_id',$partner_id)->first();
		if($partner->stara_sifra != null && $partner->stara_sifra != ''){
			$status = $partner->stara_sifra;
		}else{
			$status = "K";
		}

		$response = self::kartica_kupca(Session::get('wings_token_'.B2bOptions::server()),intval($partner->id_is),$status);
        if(!isset($response->errors) && isset($response->data)){
        	$items = $response->data;
            $resultKartica = self::kartica_kupca_body($items,$partner->partner_id);
            if(isset($resultKartica->body) && $resultKartica->body != ''){
            	self::kartica_kupca_insert_update($resultKartica->body,$partner->partner_id);
            }
        }

	}

}