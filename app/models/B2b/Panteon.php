<?php

class Panteon {

    public static function addOrderStavka($partner_id,$web_order_id,$note,$article_id,$article_code,$quantity,$amount,$pdv,$measure,$item_number,$name,$avans,$isporuka){
        $sql = "INSERT INTO _b2bOrder (narudzbina_id,kupac_id,datum,napomena,id_artikla,sifra_artikla,kolicina,cena,pdv,jedinica_mere, broj_stavke,opis,acImportFlag,avans,isporuka) VALUES ('".strval($web_order_id)."','".strval($partner_id)."','".date("Y-m-d H:i:s")."','".$note."','".strval($article_id)."','".strval($article_code)."',".strval($quantity).",".strval($amount).",".strval($pdv).",'".strval($measure)."',".strval($item_number).",'".strval($name)."','false',".strval($avans).",".strval($isporuka).")";
        DB::connection('sqlsrv')->statement($sql);
    }
    public static function orderConfirm(){
        DB::connection('sqlsrv')->statement("EXEC _pB2BOrderCreate");
    }

    public static function createOrder($cartItems,$note=''){
        $success = false;
        $order_id = 1;
        $orderIdArr = DB::select("SELECT (last_value) as web_b2b_narudzbina_id FROM web_b2b_narudzbina_web_b2b_narudzbina_id_seq");
        if(count($orderIdArr) > 0 && !is_null($orderIdArr[0]->web_b2b_narudzbina_id)){
            $order_id = $orderIdArr[0]->web_b2b_narudzbina_id;
        }
        // $cartItems = DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_id',$order_id)->get();

        $kurs = DB::table('kursna_lista')->where('kursna_lista_id',DB::table('kursna_lista')->max('kursna_lista_id'))->pluck('ziralni');
        $partner_id = Session::get('b2b_user_'.B2bOptions::server());
        $partner = DB::table('partner')->where('partner_id',$partner_id)->whereNotNull('id_is')->first();
        
        if(!is_null($partner)){
            $troskovi = B2bBasket::troskovi($order_id);
            $orderTotal = B2bBasket::orderTotal($order_id);

            foreach($cartItems as $stavka){
                $roba = DB::table('roba')->where('roba_id',$stavka->roba_id)->first();
                $stavka_id = $roba->id_is;
                $stavka_sifra = $roba->sifra_is;
                $pdv = DB::table('tarifna_grupa')->where('tarifna_grupa_id',$stavka->tarifna_grupa_id)->pluck('porez');
                $jedinica_mere = DB::table('jedinica_mere')->where('jedinica_mere_id',$roba->jedinica_mere_id)->pluck('naziv');

                self::addOrderStavka($partner->id_is,$order_id,$note,$stavka_id,$stavka_sifra,$stavka->kolicina,$stavka->jm_cena,$pdv,$jedinica_mere,$stavka->broj_stavke,$roba->naziv_web,$orderTotal->avans,$troskovi);
            }
            
            self::orderConfirm();

            $success = true;
        }
        return (object) array('success'=>$success, 'order_id'=>$order_id);
    }

}