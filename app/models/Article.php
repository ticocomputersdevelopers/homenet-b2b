<?php

class Article extends Eloquent
{
    protected $table = 'roba';

    protected $primaryKey = 'roba_id';
    
    public $timestamps = false;

	protected $hidden = array(
		'roba_id',
    	'naziv',
    	'naziv_web',
    	'grupa_pr_id',
		'sifra_d', 
	    'naziv_dopunski', 
	    'kat_broj', 
	    'broj_proizvodjaca', 
	    'tarifna_grupa_id',
	    'jedinica_mere_id',
	    'proizvodjac_id', 
	    'klasa_pr_id',
	    'sifra_k',
	    'naziv_racun_18', 
	    'naziv_racun_32', 
	    'naziv_displej', 
	    'flag_ambalaza',
	    'ambalaza_id',
	    'ambalaza',
	    'flag_dozvoljen_minus',
	    'flag_razlomljeno',
	    'tezinski_faktor',
	    'cenovnik_jm_id',
	    'trans_pak',
	    'kom_pak',
	    'valuta',
	    'garancija',
	    'flag_obavezno_markiranje',
	    'flag_prikazi_u_cenovniku',
	    'flag_extra_ponuda',
	    'napomena', 
	    'flag_aktivan',
	    'racunska_cena_nc',
	    'racunska_cena_a',
	    'racunska_cena_end',
	    'flag_usluga',
	    'opis', 
	    'mpcena', 
	    'flag_zakljucan', 
	    'web_marza',
	    'tip_cene', 
	    'flag_cenovnik',
	    'stara_grupa_id', 
	    'stari_konto', 
	    'stari_pdv', 
	    'roba_id_tico', 
	    'a_marza',
	    'mp_marza',
	    'end_marza',
	    'grupa_pr_id_kupindo',
	    'dobavljac_id',
	    'web_cena',
	    'web_flag_obavezan_prikaz',
	    'web_minimalna_kolicina',
	    'web_opis', 
	    'web_karakteristike', 
	    'web_vrsta_prikaza', 
	    'web_flag_karakteristike',
	    'akcija_flag_primeni',
	    'akcija_popust',
	    'akcija_opis', 
	    'akcija_naslov', 
	    'akcija_slika_akcija', 
	    'akcija_blok', 
	    'promena',
	    'flag_promena_connect',
	    'barkod', 
	    'akcija_stara_cena',
	    'akcija_redni_broj',
	    'valuta_id',
	    'roba_flag_cene_id',
	    'parrent_roba_id',
	    'model', 
	    'description', 
	    'keywords',
	    'pregledan_puta',
	    'b2b_max_rabat',
	    'b2b_akcijski_rabat',
	    'akcijska_cena',
	    'pregledan_puta_b2b',
	    'sku', 
	    'datum_akcije_od', 
	    'datum_akcije_do', 
	    'osobine', 
	    'seo', 
	    'sifra_is', 
	    'id_is',
		'rbr',
	    'b2b_akcija_flag_primeni',
	    'stara_cena',
	    'produzena_garancija',
	    'labela',
	    'b2b_akcija_redni_broj',
	    'b2b_datum_akcije_od',
	    'b2b_datum_akcije_do',
	    'b2b_akcijska_cena',
	    'b2b_akcija_popust',
	    'update_at',
	    'bodovi_popust',
	    'design_id'
    	);

	public function getIdAttribute()
	{
	    return isset($this->attributes['roba_id']) ? $this->attributes['roba_id'] : null;
	}
	public function getNameAttribute()
	{
	    return isset($this->attributes['naziv']) ? $this->attributes['naziv'] : null;
	}
	public function getWebNameAttribute()
	{
	    return isset($this->attributes['naziv_web']) ? $this->attributes['naziv_web'] : null;
	}
	public function getCategoryIdAttribute()
	{
	    return isset($this->attributes['grupa_pr_id']) ? $this->attributes['grupa_pr_id'] : null;
	}
	public function getBrandIdAttribute()
	{
	    return isset($this->attributes['proizvodjac_id']) ? ($this->attributes['proizvodjac_id'] > 0 ? $this->attributes['proizvodjac_id'] : null) : null;
	}
	public function getActiveAttribute()
	{
	    return isset($this->attributes['flag_aktivan']) ? $this->attributes['flag_aktivan'] : 0;
	}
	public function getShowAttribute()
	{
	    return isset($this->attributes['flag_prikazi_u_cenovniku']) ? $this->attributes['flag_prikazi_u_cenovniku'] : 0;
	}
	public function getShowB2bAttribute()
	{
	    return isset($this->attributes['flag_cenovnik']) ? $this->attributes['flag_cenovnik'] : 0;
	}
	public function getLockedAttribute()
	{
	    return isset($this->attributes['flag_zakljucan']) ? ($this->attributes['flag_zakljucan'] ? 1 : 0) : 0;
	}
	public function getTypeIdAttribute()
	{
	    return isset($this->attributes['tip_cene']) ? $this->attributes['tip_cene'] : null;
	}
	public function getVatIdAttribute()
	{
	    return isset($this->attributes['tarifna_grupa_id']) ? $this->attributes['tarifna_grupa_id'] : null;
	}
	public function getMeasureIdAttribute()
	{
	    return isset($this->attributes['jedinica_mere_id']) ? $this->attributes['jedinica_mere_id'] : null;
	}
	public function getHtmlDescriptionAttribute()
	{
	    return isset($this->attributes['web_opis']) ? $this->attributes['web_opis'] : null;
	}
	public function getBasicB2cPriceAttribute()
	{
	    return isset($this->attributes['racunska_cena_nc']) ? floatval($this->attributes['racunska_cena_nc']) : 0.00;
	}
	public function getBasicB2bPriceAttribute()
	{
	    return isset($this->attributes['racunska_cena_end']) ? floatval($this->attributes['racunska_cena_end']) : 0.00;
	}
	public function getWebPriceAttribute()
	{
	    return isset($this->attributes['web_cena']) ? floatval($this->attributes['web_cena']) : 0.00;
	}
	public function getRetailPriceAttribute()
	{
	    return isset($this->attributes['mpcena']) ? floatval($this->attributes['mpcena']) : 0.00;
	}
	public function getSalePriceAttribute()
	{
	    return isset($this->attributes['akcijska_cena']) && $this->attributes['akcijska_cena'] > 0 ? floatval($this->attributes['akcijska_cena']) : (isset($this->attributes['web_cena']) ? floatval($this->attributes['web_cena']) : 0.00);
	}
	public function getWebMarginAttribute()
	{
	    return isset($this->attributes['web_marza']) ? floatval($this->attributes['web_marza']) : 0.00;
	}
	public function getRetailMarginAttribute()
	{
	    return isset($this->attributes['mp_marza']) ? floatval($this->attributes['mp_marza']) : 0.00;
	}
	public function getTagsListAttribute()
	{
	    return isset($this->attributes['tags']) ? floatval($this->attributes['tags']) : null;
	}
    public function getExternalCodeAttribute()
    {
        return isset($this->attributes['id_is']) && !empty($this->attributes['id_is']) ? $this->attributes['id_is'] : null;
    }

	protected $appends = array(
    	'id',
    	'name',
    	'web_name',
    	'category_id',
    	'brand_id',
    	'active',
    	'show',
    	'show_b2b',
    	'locked',
    	'type_id',
    	'vat_id',
    	'measure_id',
    	'html_description',
    	'basic_b2c_price',
    	'basic_b2b_price',
    	'web_price',
    	'retail_price',
    	'sale_price',
    	'web_margin',
    	'retail_margin',
    	'tags_list',
    	'external_code'
    	);

    public function articleLangs(){
        return $this->hasMany('ArticleLang', 'roba_id');
    }

    public function category(){
        return $this->belongsTo('Category','grupa_pr_id');
    }

    public function brand(){
        return $this->belongsTo('Brand','proizvodjac_id');
    }

    public function type(){
        return $this->belongsTo('Type','tip_cene');
    }

    public function vat(){
        return $this->belongsTo('Vat','tarifna_grupa_id');
    }

    public function measureUnit(){
        return $this->belongsTo('MeasureUnit','jedinica_mere_id');
    }

    public function measure(){
        return $this->measureUnit()->where('jedinica_mere_id','>',-1);
    }

    public function stock(){
        return $this->hasMany('Stock', 'roba_id');
    }

    public function images(){
        return $this->hasMany('Images','roba_id');
    }

    public function mainImages(){
        return $this->images()->whereNull('parent_id');
    }

    public function comments(){
        return $this->hasMany('ArticleComment','roba_id');
    }

}