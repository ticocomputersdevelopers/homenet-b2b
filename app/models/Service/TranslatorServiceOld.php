<?php 
namespace Service;

use Google\Cloud\Core\ServiceBuilder;
use Language;
use AdminOptions;


class TranslatorServiceOld {
	const CIRILIC = 'cir';
	private $enableGoogle;
	private $translator;
	private $fromLang;
	private $toLang;

	public function __construct($fromLang,$toLang){
		$this->fromLang = $fromLang;
		$this->toLang = $toLang;
		$this->enableGoogle = $fromLang != self::CIRILIC && $toLang != self::CIRILIC && AdminOptions::gnrl_options(3038) == 1 && (!is_null(AdminOptions::gnrl_options(3037,'str_data')) && AdminOptions::gnrl_options(3037,'str_data') != '');

		if($this->enableGoogle){
			$builder = new ServiceBuilder([
			    'key' => AdminOptions::gnrl_options(3037,'str_data')
			]);
			$this->translator = $builder->translate();
		}
	}

	public function translate($string){
		if(!is_null($string) && $string != ''){
			if($this->enableGoogle){
				$translation = $this->translator->translate($string, [
				    'source' => $this->fromLang,
				    'target' => $this->toLang
				]);
				$string = html_entity_decode($translation['text']);

				if($this->toLang == 'sr'){
					$string = Language::cir_to_latin($string);
				}
			}else{
				if($this->fromLang == 'sr' && $this->toLang == self::CIRILIC){

					$string = Language::latin_to_cir(strip_tags($string));
				}elseif($this->fromLang == self::CIRILIC && $this->toLang == 'sr'){
					$string = Language::cir_to_latin(strip_tags($string));
				}
			}
		}
		return $string;

	}

}