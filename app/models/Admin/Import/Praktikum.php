<?php
namespace Import;
use Import\Support;
use DB;
use File;
use PHPExcel; 
use PHPExcel_IOFactory;

class Praktikum {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){
		if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/praktikum/praktikum.xls');
			$products_file = "files/praktikum/praktikum.xls";
			$continue = true;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();
	        
	        for ($row = 1; $row <= $lastRow; $row++) {
	            $sifra = $worksheet->getCell('B'.$row)->getValue();
	            $grupa = $worksheet->getCell('D'.$row)->getValue();
	            $pisac = $worksheet->getCell('G'.$row)->getValue();
	            $naziv = $worksheet->getCell('F'.$row)->getValue();
	            $cena_nc = $worksheet->getCell('H'.$row)->getValue();
	            $opis = $worksheet->getCell('K'.$row)->getValue();
	            $slika = $worksheet->getCell('J'.$row)->getValue();
	            
				if(isset($sifra) && isset($cena_nc) && is_numeric($cena_nc)){			

					$sPolja = '';
					$sVrednosti = '';
					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . Support::encodeTo1250(pg_escape_string(trim($sifra))) . "',";
					$sPolja .= " naziv,";					$sVrednosti .= " '" . Support::encodeTo1250(pg_escape_string($naziv)) . "',";
					$sPolja .= " kolicina,";				$sVrednosti .= " " . number_format(1, 2,'.','') . ",";
					$sPolja .= " grupa,";					$sVrednosti .= " '" . Support::encodeTo1250(pg_escape_string($grupa)) ."',";
					$sPolja .= " proizvodjac,";				$sVrednosti .= " '" . Support::encodeTo1250(pg_escape_string($pisac)) ."',";
					$sPolja .= " opis,";					$sVrednosti .= " '" . Support::encodeTo1250(pg_escape_string($opis)) ."',";
					if(!empty($opis) ){
					$sPolja .= " flag_opis_postoji,";		$sVrednosti .= " 1,";
					}
					if(!empty($slika) ){
					$sPolja .= " flag_slika_postoji,";		$sVrednosti .= " 1,";
					}
					$sPolja .= " cena_nc";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric(floatval($cena_nc),1,$kurs,$valuta_id_nc),2, '.', '') . "";

					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");			
					if(!empty($slika)){
						DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp (partner_id,sifra_kod_dobavljaca,putanja,akcija) VALUES(".$dobavljac_id.",'".$sifra."','".$slika."',1 )");	
					}
				}
			}

			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/praktikum/praktikum.xls');
			$products_file = "files/praktikum/praktikum.xls";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();

	        for ($row = 1; $row <= $lastRow; $row++) {
	            $sifra = $worksheet->getCell('B'.$row)->getValue();
	            $grupa = $worksheet->getCell('D'.$row)->getValue();
	            $pisac = $worksheet->getCell('G'.$row)->getValue();
	            $naziv = $worksheet->getCell('F'.$row)->getValue();
	            $cena_nc = $worksheet->getCell('H'.$row)->getValue();
	            $opis = $worksheet->getCell('K'.$row)->getValue();
	            $slika = $worksheet->getCell('J'.$row)->getValue();
	            
				if(isset($sifra) && isset($cena_nc) && is_numeric($cena_nc)){			

					$sPolja = '';
					$sVrednosti = '';
					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . Support::encodeTo1250(pg_escape_string(trim($sifra))) . "',";
					$sPolja .= " kolicina,";				$sVrednosti .= " " . number_format(1, 2,'.','') . ",";
					$sPolja .= " cena_nc";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric(floatval($cena_nc),1,$kurs,$valuta_id_nc),2, '.', '') . "";

					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");			

				}
			}

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}


}