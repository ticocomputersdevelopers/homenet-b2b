<?php

class MeasureUnit extends Eloquent
{
    protected $table = 'jedinica_mere';

    protected $primaryKey = 'jedinica_mere_id';
    
    public $timestamps = false;

	protected $hidden = array(
            'jedinica_mere_id',
            'naziv',
            'naziv_en',
            'osnovna_jedinica_mere_id',
            'faktor',
            'oznaka',
            'sifra_connect'
    	);

	public function getIdAttribute()
	{
	    return isset($this->attributes['jedinica_mere_id']) ? $this->attributes['jedinica_mere_id'] : null;
	}
	public function getNameAttribute()
	{
	    return isset($this->attributes['naziv']) ? $this->attributes['naziv'] : null;
	}

	protected $appends = array(
    	'id',
    	'name'
    	);

    public function articles(){
        return $this->hasMany('Article', 'jedinica_mere_id');
    }

    public function getIdByName($name,$mapped=[]){
        if(isset($mapped[$name])){
            return $mapped[$name];
        }
        
        if(is_null($typeId = self::where('naziv',$name)->pluck('jedinica_mere_id'))){
            $nextId = self::max('jedinica_mere_id')+1;

            $type = new MeasureUnit();
            $type->jedinica_mere_id = $nextId;
            $type->naziv = $name;
            $type->save();
            $typeId = $type->jedinica_mere_id;
        }

        return $typeId;
    }

    public function mappedByName(){
        $mapped = [];
        foreach(self::select('jedinica_mere_id','naziv')->get() as $measure){
            $mapped[$measure->naziv] = $measure->jedinica_mere_id;
        }
        return $mapped;
    }

}