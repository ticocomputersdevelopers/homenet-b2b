
/*======= FIXED HEADER ==========*/ 
var header = $('header'),
	header_height = $('header').height(),
	offset_top = $('header').offset().top,
	offset_bottom = offset_top + header.outerHeight(true),
	admin_menu = $('#admin-menu');


$(window).scroll(function(){
 
 	if ($(window).width() > 1024 ) {
 
	  	if ($(window).scrollTop() >= offset_bottom) {

	  		if(admin_menu.length){ $('.JSsticky_header').css('top', admin_menu.outerHeight() + 'px'); }

			$('#JSfixed_header').addClass('JSsticky_header');

			header.css('height', header_height + 'px');	      
			
		
				$('.details').hide();
				$(".purple").hide();
				$('.gray').css('background', 'unset')
				$(".search > div").removeClass('col-lg-6').addClass('col-lg-7');
				$(".changeCol").removeClass('col-lg-7').addClass('col-lg-4')
				$('.search').css('padding', '14px 5px' ).addClass('linear-gradient')
			

	    }else {

			$('#JSfixed_header').removeClass('JSsticky_header');    
			
			header.css('height', '');

			$('.details').show();
			$(".purple").show();
			$('.search').css('padding', 'unset' ).addClass("linear-gradient")
	    }
	}
});    
 
 