var ajax_load = false;
var max_page = 1;
var products_page = 1;
var products_grupa_pr_id = 0;
var products_na_webu = null;
var products_na_akciji = null;
var products_slike = null;
var products_opis = null;


$(document).ready(function(){
	//init load
	product_list(products_page,products_grupa_pr_id,products_na_webu,products_na_akciji,products_slike,products_opis);

	//select all
	$(document).on('click','#JSRobaSelected',function(e){
		if(!$(this).attr("checked")){
			$(this).attr('checked',true);
			$(this).prop('checked',true);
			$(".JSRobaSelected").attr('checked',true);
			$(".JSRobaSelected").prop("checked",true);
		}else{
			$(this).attr('checked',false);
			$(this).prop('checked',false);
			$(".JSRobaSelected").attr('checked',false);
			$(".JSRobaSelected").prop("checked",false);
		}
		$('#JSFAProductListSelectCount').text($(".JSRobaSelected[checked='checked']").length);
	});
	$(document).on('click','.JSRobaSelected',function(){
		if($(this).attr("checked")){
			$(this).attr('checked',false);
			$(this).prop('checked',false);
		}else{
			$(this).attr('checked',true);
			$(this).prop('checked',true);
		}
		$('#JSFAProductListSelectCount').text($(".JSRobaSelected[checked='checked']").length);
	});	


	//filter change grupa
	$('#JSFAGrupaSelect').change(function(){
		max_page = 1;
		products_page = 1;
		products_grupa_pr_id = $(this).val();
		$('#JSFAProductListSelectCount').text('0');
		$('#JSFAArticleSearch').val('');
		$('#JSFAProductListContent').html('');
		product_list(products_page,products_grupa_pr_id,products_na_webu,products_na_akciji,products_slike,products_opis);
	});
	//filter on web
	$('#JSFANaWebuSelect').change(function(){
		max_page = 1;
		products_page = 1;
		products_na_webu = $(this).val();
		$('#JSFAProductListSelectCount').text('0');
		$('#JSFAArticleSearch').val('');
		$('#JSFAProductListContent').html('');
		product_list(products_page,products_grupa_pr_id,products_na_webu,products_na_akciji,products_slike,products_opis);
	});
	//filter on sale
	$('#JSFANaAkcijiSelect').change(function(){
		max_page = 1;
		products_page = 1;
		products_na_akciji = $(this).val();
		$('#JSFAProductListSelectCount').text('0');
		$('#JSFAArticleSearch').val('');
		$('#JSFAProductListContent').html('');
		product_list(products_page,products_grupa_pr_id,products_na_webu,products_na_akciji,products_slike,products_opis);
	});
	//filter slike
	$('#JSFASlikeSelect').change(function(){
		max_page = 1;
		products_page = 1;
		products_slike = $(this).val();
		$('#JSFAProductListSelectCount').text('0');
		$('#JSFAArticleSearch').val('');
		$('#JSFAProductListContent').html('');
		product_list(products_page,products_grupa_pr_id,products_na_webu,products_na_akciji,products_slike,products_opis);
	});
	//filter opis
	$('#JSFAOpisSelect').change(function(){
		max_page = 1;
		products_page = 1;
		products_opis = $(this).val();
		$('#JSFAProductListSelectCount').text('0');
		$('#JSFAArticleSearch').val('');
		$('#JSFAProductListContent').html('');
		product_list(products_page,products_grupa_pr_id,products_na_webu,products_na_akciji,products_slike,products_opis);
	});

	//search
	$('#JSFAArticleSearch').keyup(function(){
		var search_string = $(this).val();

		max_page = 1;
		products_page = 1;
		products_grupa_pr_id = 0;
		products_na_webu = null;
		products_na_akciji = null;
		products_slike = null;
		products_opis = null;
		$('#JSFAProductListSelectCount').text('0');
		$('#JSFAProductListContent').html('');
		product_list(products_page,products_grupa_pr_id,products_na_webu,products_na_akciji,products_slike,products_opis,search_string);
	});

	//scroll event
	$('#JSFAProductListContent').scroll(function(){
		var scroll_bottom = $(this)[0].scrollHeight - ($(this).scrollTop() + $(this).innerHeight());
		if(scroll_bottom < 300 && !ajax_load && max_page > products_page){
			products_page++;
			product_list(products_page,products_grupa_pr_id,products_na_webu,products_na_akciji,products_slike,products_opis,$('#JSFAArticleSearch').val());
		}
	});

	$('#JSFAActionExecute').click(function(){
		var action_name = $('#JSFAActionSelect').val();
		var selected_roba_ids = get_selected_roba_ids();
		var confirm_action = true;

		if(selected_roba_ids.length > 0){
			if(action_name == 'delete'){
				confirm_action = false;

				swal("Arkali će biti obrisani. Da li ste sigurni?", {
				  buttons: {
				    no: {
				      text: "Ne",
				      value: false,
				    },
				    yes: {
				      text: "Da",
				      value: true,
				    }
				  },
				}).then(function(value){
					if(value){
						$.post('/admin/front-admin-product-list-execute', { action: action_name, roba_ids: selected_roba_ids }, function(response) {

							if(response.mesages == ''){							
								swal('Data akcija je uspešno izvršena.', "", "success");
								setTimeout(function() { sweetAlert.close(); }, 2000);
							}else{
								swal(response.mesages+"\nIpak obriši?", {
								  buttons: {
								    no: {
								      text: "Ne",
								      value: false,
								    },
								    yes: {
								      text: "Da",
								      value: true,
								    }
								  },
								}).then(function(value_check){
									if(value_check){
										$.post('/admin/front-admin-product-list-execute', { action: action_name, roba_ids: selected_roba_ids, check: 'true' }, function(response) {
											$('#JSFAProductListSelectCount').text('0');
											$('#JSFAProductListContent').html('');
											product_list(products_page,products_grupa_pr_id,products_na_webu,products_na_akciji,products_slike,products_opis);
										});
									}
								});							
							}
							$('#JSFAProductListSelectCount').text('0');
							$('#JSFAProductListContent').html('');
							product_list(products_page,products_grupa_pr_id,products_na_webu,products_na_akciji,products_slike,products_opis);
						});
					}
				});				
			}

			if(confirm_action){
				$.post('/admin/front-admin-product-list-execute', { action: action_name, roba_ids: selected_roba_ids }, function(response) {
					//refresh products list
					if((products_na_webu == 0 && action_name == 'web_on')
						|| (products_na_webu == 1 && action_name == 'web_off')
						|| (products_na_akciji == 0 && action_name == 'sell_on')
						|| (products_na_akciji == 1 && action_name == 'sell_off')){

						$('#JSFAProductListSelectCount').text('0');
						$('#JSFAProductListContent').html('');
						product_list(products_page,products_grupa_pr_id,products_na_webu,products_na_akciji,products_slike,products_opis);
					}else{ //update products list
						$.each(selected_roba_ids, function(index,value) {
							var executed_element = $(".JSFAProductOnGrid[data-roba_id='"+value+"']");

							if(action_name == 'web_on'){
								executed_element.find('.JSFAflag_prikazi_u_cenovniku').text('DA');
							}else if(action_name == 'web_off'){
								executed_element.find('.JSFAflag_prikazi_u_cenovniku').text('NE');
							}
						});
					}

					swal('Data akcija je uspešno izvršena.', "", "success");
					setTimeout(function() { sweetAlert.close(); }, 2000);
				});
			}
		}else{
			swal('Nemate selektovane artikle.', "", "error");
			setTimeout(function() { sweetAlert.close(); }, 2000);
		}
	});

	$(document).on('click','#JSFAProductNewModalCall',function(){
		$('#FAProductModal').modal('show');
		product(0);
	});

	$(document).on('click','.JSFAProductModalCall',function(){
		var roba_id = $(this).data('roba_id');
		$('#FAProductModal').modal('show');
		product(roba_id);
	});

});


function product_list(page,grupa_pr_id,na_webu,na_akciji,slike,opis,search_string=''){
	if(!ajax_load){
		ajax_load = true;
		$.post('/admin/front-admin-product-list', {page:page, grupa_pr_id:grupa_pr_id, na_webu: na_webu, na_akciji: na_akciji, slike:slike, opis:opis, search_string:search_string}, function (response){
			$('#JSFAProductListContent').append(response.content);
			$('#JSFAProductListCount').text(response.count);

			$("#JSRobaSelected").attr('checked',false);
			$("#JSRobaSelected").prop("checked",false);

			ajax_load = false;
			max_page = response.max_page;
		});
	}
}

function product(roba_id){
	tinymce.remove("textarea#JSFAweb_opis");
	$('#JSFAProductContent').html('');
	$.post('/admin/front-admin-product', { roba_id: roba_id }, function(response) {
		$('#JSFAProductContent').html(response.content);
		//web opis
	    tinymce.init({
	        selector: "textarea#JSFAweb_opis",
	        height : "400px",
	        plugins: [
	            "advlist autolink lists link image charmap print preview anchor",
	            "searchreplace visualblocks code fullscreen",
	            "insertdatetime media table contextmenu paste",
	            "autoresize"
	        ],
	        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
	    });
		$("#datum_akcije_od, #datum_akcije_do").datepicker();
		$('#datum_akcije_od').keydown(false);
		$('#datum_akcije_do').keydown(false);
	});
}

function get_selected_roba_ids(){
	return $(".JSRobaSelected[checked='checked']").map(function(){
	    return $(this).data('roba_id');
	}).get();
}
