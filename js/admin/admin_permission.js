$(document).ready(function () {
	var curr_loc_arr = location.pathname.split('/');

 	var datepicker_from = curr_loc_arr[3];
 	var datepicker_to = curr_loc_arr[4];
 	var adminUser = curr_loc_arr[5];
 	var mainAction = curr_loc_arr[6];
 	var statistika = curr_loc_arr[7];

	$(function() {
		$( "#datepicker_from, #datepicker_to" ).datepicker();
	});

	$('#datepicker_from').keydown(false);
	$('#datepicker_to').keydown(false);

	//
	$('#datepicker_from').change(function() {
	 	datepicker_from = $(this).val(); 
	 	
		if(datepicker_to != '' && datepicker_from != ''){
			window.location.href = base_url + 'admin/logovi/' + datepicker_from + '/' + datepicker_to + '/' + adminUser + '/' + mainAction + '/' + statistika;
		}
	});
	$('#datepicker_to').change(function() {
	 	datepicker_to = $(this).val();

		if(datepicker_to != '' && datepicker_from != ''){
			window.location.href = base_url + 'admin/logovi/' + datepicker_from + '/' + datepicker_to + '/' + adminUser + '/' + mainAction + '/' + statistika;
		}
	});

	$('#loging').on('change', function() {
		var data = $.parseJSON($(this).val());
		adminUser = data.id;
		datepicker_from = data.dateFrom;
		datepicker_to = data.dateTo;
		
		window.location.href = base_url + 'admin/logovi/' + datepicker_from + '/' + datepicker_to + '/' + adminUser + '/' + mainAction + '/' + statistika;
	});

	$('#adminUser').on('change', function() {
		adminUser = $(this).val();
			window.location.href = base_url + 'admin/logovi/' + datepicker_from + '/' + datepicker_to + '/' + adminUser + '/' + mainAction + '/' + statistika;
	});

	$('#mainAction').on('change', function() {
		mainAction = $(this).val();
			window.location.href = base_url + 'admin/logovi/' + datepicker_from + '/' + datepicker_to + '/' + adminUser + '/' + mainAction + '/' + statistika;
	});

	$('.statistika').on('change', function(){
		statistika = $(this).val();
		if(statistika != -1){
			window.location.href = base_url + 'admin/logovi/' + datepicker_from + '/' + datepicker_to + '/' + adminUser + '/' + mainAction + '/' + statistika;
		}else if(statistika == -1){
			window.location.href = base_url + 'admin/logovi/' + datepicker_from + '/' + datepicker_to + '/' + adminUser + '/' + mainAction;			
		}
	});

	$('.LogovanjaNaziv').click(function() {
		if ($(this).find("td:eq(1)").text() != 'Logovanje' && $(this).find("td:eq(1)").text() != 'Log out' 
		&& $(this).find("td:eq(1)").text() != 'B2B Brisanje uploadovanih slika kod stranica'
		&& $(this).find("td:eq(1)").text() != 'B2B Postavljanje slike kod stranica'
		&& $(this).find("td:eq(1)").text() != 'B2B Baneri dodavanje slike'
		&& $(this).find("td:eq(1)").text() != 'B2B Baneri dodavanje linkova'
		&& $(this).find("td:eq(1)").text() != 'B2B Baneri brisanje slika i linkova'
		&& $(this).find("td:eq(1)").text() != 'Konfigurisanje SWIFT mailer-a'
		&& $(this).find("td:eq(1)").text() != 'Podešavanja boje prodavnice'
		&& $(this).find("td:eq(1)").text() != 'Podešavanja swift mejlera - aktivan/neaktivan'
		&& $(this).find("td:eq(1)").text() != 'Podešavanja google analitike'
		&& $(this).find("td:eq(1)").text() != 'Podešavanja facebook piksela'
		&& $(this).find("td:eq(1)").text() != 'Podešavanja četa'
		&& $(this).find("td:eq(1)").text() != 'Podešavanja ključeva za intesu'
		&& $(this).find("td:eq(1)").text() != 'Generisanje Sitemapa'
		&& $(this).find("td:eq(1)").text() != 'Backup artikala'
		&& $(this).find("td:eq(1)").text() != 'Backup slika artikala'
		&& $(this).find("td:eq(1)").text() != 'Import artikala'
		&& $(this).find("td:eq(1)").text() != 'Podešavanja boje prodavnice'
		&& $(this).find("td:eq(1)").text() != 'Podešavanja boje prodavnice'
		&& $(this).find("td:eq(1)").text() != 'Podešavanja boje prodavnice') {
			var id = $(this).data('id');
			if($(this).data('isopen') == 'closed'){
				$(this).data('isopen','opened');
				$(".LogovanjaProizvodi[data-id='"+id+"']").removeAttr('hidden');
			}else{
				$(this).data('isopen','closed');
				$(".LogovanjaProizvodi[data-id='"+id+"']").attr('hidden','hidden');
			}
		}
	});

	$('.Akcija').click(function() {
			var id = $(this).data('id');
			if($(this).data('isopen') == 'closed'){
				$(this).data('isopen','opened');
				$(".AkcijaData[data-id='"+id+"']").removeAttr('hidden');
			}else{
				$(this).data('isopen','closed');
				$(".AkcijaData[data-id='"+id+"']").attr('hidden','hidden');
			}
	});


	$('.JSGlavni, .JSSporedni').click(function(){

		var execute;
		if($(this).attr('checked')){
			$(this).removeAttr('checked');
			execute = 0;
		}else{
			$(this).attr('checked',true);
			execute = 1;
		}

		if($(this).hasClass('JSGlavni')){
			$('.JSSporednaKolona').hide();
		}

		var general_module = $(this).data('general-module');
		var data = {
			action: 'modul',
			execute: execute,
			group_id: parseInt(general_module.group),
			module_id: parseInt(general_module.module_id),
			tip: parseInt(general_module.tip)
		};

		$.post(base_url+'admin/ajax/permissions',data, function (response){});
});

	$('.JSGlavniIskljuci').on('change', function(){
		var execute;
		if($(this).prop('checked')){
			$('.JSSporednaKolona').hide();
			$(this).siblings().attr('checked', false);
			$(this).closest('div').find('.JSGlavni').prop('checked', false);
			$(this).prop("disabled", true);
			$(this).siblings().prop("disabled", false);
			execute = 0;
			var general_module = $(this).data('general-module');
			var data = {
				execute: execute,
				group_id: parseInt(general_module)
			};
			$.post(base_url+'admin/ajax/permissionsAll',data, function (response){});
		}
	});
	$('.JSGlavniUkljuci').on('change', function(){
		var execute;
		if($(this).prop('checked')){
			$('.JSSporednaKolona').hide();
			$(this).siblings().attr('checked', false);
			$(this).closest('div').find('.JSGlavni').prop('checked', true);
			$(this).prop("disabled", true);
			$(this).siblings().prop("disabled", false);
			execute = 1;
			var general_module = $(this).data('general-module');
			var data = {
				execute: execute,
				group_id: parseInt(general_module)
			};
			$.post(base_url+'admin/ajax/permissionsAll',data, function (response){});
		}
	});

});
